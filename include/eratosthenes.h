#ifndef _ERATOSTHENES_H_
#define _ERATOSTHENES_H_

#include <vector>
#include <iostream>
#include <cmath>
#include "include/bignum.h"

class Eratosthene {
 public:
  std::vector<int> m_primes;

  Eratosthene();
  ~Eratosthene();
  
  void gen_primes(unsigned bound);
  std::vector<int> form_factor_base(const unsigned & B, const BigNumber & N);

 private:
  unsigned m_bound;
};

#endif /* _ERATOSTHENES_H_ */
