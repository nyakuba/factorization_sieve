#ifndef _SIEVE_H_
#define _SIEVE_H_

#include <iostream>
#include <vector>
#include <assert.h>
#include <cmath>
#include <pthread.h>
#include "include/bignum.h"
#include "include/sparse.h"
#include "include/gfelem.h"
#define BNZero BigNumber::Zero()
#define BNOne BigNumber::One()
#define BNTwo BigNumber::Two()

//for testing
namespace sieve_test {
  struct testing;
}

class Sieve {
 public:
  Sieve();
  Sieve(const BigNumber & N);
  ~Sieve();

  static const unsigned MAX_SIEVE_INTERVAL = 5000000;

  void setN(BigNumber & N);

  void get_smooth_exp(const GF & gf, const unsigned & B, 
		      const BigNumber & N, const std::vector<BigNumber> & fb, 
		      std::vector<BigNumber> & smooth, SMatrix & exponents);
  
  void gen_smooth_pdiv(const BigNumber & B, const BigNumber & a, const BigNumber & b,
		       const std::vector<BigNumber> & factor_base, 
		       std::vector<BigNumber> & smooth,
		       std::vector<std::vector<int>> & exponents);
  void gen_smooth_qsieve_factor_out(const BigNumber & B, const BigNumber & a, const BigNumber & b,
				    const std::vector<BigNumber> & factor_base, 
				    std::vector<BigNumber> & smooth);
  void gen_smooth_qsieve_log(const BigNumber & B, const BigNumber & a, const BigNumber & b,
			     const std::vector<BigNumber> & factor_base, 
			     std::vector<BigNumber> & smooth,
			     std::vector<std::vector<int>> & exponents);
  void gen_smooth_mpoly(const BigNumber & B, const BigNumber & a, const BigNumber & b,
			const std::vector<BigNumber> & factor_base, 
			std::vector<BigNumber> & smooth);
  void gen_smooth_mp(const int & B, const std::vector<int> & fb,
		     const std::vector<BigNumber> & tsh_fb,
		     const BigNumber pa, Gen & gen,
		     std::vector<BigNumber> & smooth,
		     std::vector<BigNumber> & acoeff,
		     std::vector<std::vector<int>> & exponents) const;

  static bool pdiv(const BigNumber & a, 
		   const std::vector<BigNumber> & factor_base,
		   std::vector<int> & exp);

  static bool pdiv(const BigNumber & a, 
		   const std::vector<int> & factor_base,
		   std::vector<int> & exp);

  static BigNumber tonelli_shanks(const BigNumber & a, const BigNumber & p);

 private:
  static unsigned factor_out(BigNumber & a, const BigNumber & factor);
  static BigNumber mod_sqrt_bf(const BigNumber & a, const BigNumber & p);
  static BigNumber shanks(const BigNumber & a, const BigNumber & p);
  static std::vector<BigNumber> bruteforce_q(const BigNumber & a, const BigNumber & p);

  BigNumber m_N;
  BigNumber m_B;
  BigNumber m_a;
  BigNumber m_b;
  std::vector<BigNumber> m_interval;

  //for testing
  friend struct sieve_test::testing;
};

#endif /* _SIEVE_H_ */
