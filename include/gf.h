#ifndef _GF_H_
#define _GF_H_

#include <vector>

// ����� �������� �������� ���������� � ��������
//   ����, � ������� ����������� ��������.
class GF {
 public:
  GF(const unsigned & m, const unsigned & prim);
  GF(const unsigned & m, const std::vector<int> & prim_coeffs);
  ~GF();

  unsigned getM() const;
  unsigned getPrimitive() const;
  friend bool operator ==(const GF & a, const GF & b);
  friend bool operator !=(const GF & a, const GF & b);

 private:
  unsigned m_m;
  unsigned m_primitive;
};

#endif /* _GF_H_ */
