#ifndef _BIGNUMBER_H_
#define _BIGNUMBER_H_

#include <ippcp.h>
#include <iostream>
#include <vector>
#include <iterator>
#include <string>
#include <string.h>
#include <cmath>
#include <stdexcept>
#include <cstring>

class BigNumber {
  friend class Gen;

 public:
  BigNumber(Ipp32u value=0);
  BigNumber(Ipp32s value);
  BigNumber(const IppsBigNumState* pBN);
  BigNumber(const Ipp32u* pData, int length=1, IppsBigNumSGN sgn=IppsBigNumPOS);
  BigNumber(const BigNumber& bn);
  BigNumber(const char *s);
  virtual ~BigNumber();
  // set value
  void Set(const Ipp32u* pData, int length=1, IppsBigNumSGN sgn=IppsBigNumPOS);
  // conversion to IppsBigNumState
  friend IppsBigNumState* BN(const BigNumber& bn) {return bn.m_pBN;}
  operator IppsBigNumState* () const { return m_pBN; }
  // some useful constatns
  static const BigNumber& Zero();
  static const BigNumber& One();
  static const BigNumber& Two();
  // arithmetic operators probably need
  BigNumber& operator = (const BigNumber& bn);
  BigNumber& operator += (const BigNumber& bn);
  BigNumber& operator -= (const BigNumber& bn);
  BigNumber& operator *= (Ipp32u n);
  BigNumber& operator *= (const BigNumber& bn);
  BigNumber& operator /= (const BigNumber& bn);
  BigNumber& operator %= (const BigNumber& bn);
  friend BigNumber operator + (const BigNumber& a, const BigNumber& b);
  friend BigNumber operator - (const BigNumber& a, const BigNumber& b);
  friend BigNumber operator * (const BigNumber& a, const BigNumber& b);
  friend BigNumber operator * (const BigNumber& a, Ipp32u);
  friend BigNumber operator % (const BigNumber& a, const BigNumber& b);
  friend BigNumber operator / (const BigNumber& a, const BigNumber& b);
  bool isDivTwo() const;
  void bpow(const BigNumber & power);
  void bpow(unsigned pow);
  void babs();
  void bsqrt();
  friend double loge(const BigNumber & a);
  // modulo arithmetic
  BigNumber Modulo(const BigNumber& a) const;
  BigNumber ModAdd(const BigNumber& a, const BigNumber& b) const;
  BigNumber ModSub(const BigNumber& a, const BigNumber& b) const;
  BigNumber ModMul(const BigNumber& a, const BigNumber& b) const;
  BigNumber ModPow(const BigNumber& a, const BigNumber& power) const;
  BigNumber ModPow1(const BigNumber& a, const BigNumber& power) const;
  BigNumber InverseAdd(const BigNumber& a) const;
  BigNumber InverseMul(const BigNumber& a) const;
  BigNumber legendre_symbol(const BigNumber& a) const;
  friend void bgcd(const BigNumber & a, const BigNumber & b, BigNumber & gcd);
  friend void bgcdex(const BigNumber & a, const BigNumber & b, BigNumber & u, BigNumber & v, BigNumber & gcd);
  // comparisons
  bool operator < (const BigNumber& b) const;
  bool operator > (const BigNumber& b) const;
  bool operator == (const BigNumber& b) const;
  bool operator != (const BigNumber& b) const;
  bool operator <= (const BigNumber& b) const {return !(*this>b);}
  bool operator >= (const BigNumber& b) const {return !(*this<b);}
  // easy tests
  bool IsOdd() const;
  bool IsEven() const { return !IsOdd(); }
  // tests
  bool IsPrime() const;
  bool IsPerfectSquare() const;
  // size of BigNumber
  int MSB() const;
  int LSB() const;
  int BitSize() const { return MSB()+1; }
  int DwordSize() const { return (BitSize()+31)>>5;}
  friend int Bit(const std::vector<Ipp32u>& v, int n);
  // conversion and output
  unsigned uns() const;
  void num2hex( std::string& s ) const; // convert to hex string
  void num2vec( std::vector<Ipp32u>& v ) const; // convert to 32-bit word vector
  friend std::ostream& operator << (std::ostream& os, const BigNumber& a);

 protected:
  bool create(const Ipp32u* pData, int length, IppsBigNumSGN sgn=IppsBigNumPOS);
  IppStatus compare(const IppsBigNumState * pBN, Ipp32u * result) const;

  IppsBigNumState* m_pBN;
};

// convert bit size into 32-bit words
#define BITSIZE_WORD(n) ((((n)+31)>>5))
#endif // _BIGNUMBER_H_
