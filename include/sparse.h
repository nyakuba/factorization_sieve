#ifndef _SPARSE_H_
#define _SPARSE_H_

#include <vector>
#include <gfelem.h>
#include <iostream>
#include "include/rand.h"

// Старый Йельский формат.
class SMatrix {
 public:
  SMatrix(int N, int M, const GFElem & zero);
  SMatrix(int N, int M, const GFElem & zero,
	  const std::vector<GFElem> & v, 
	  const std::vector<unsigned> & r, 
	  const std::vector<unsigned> & c);
  SMatrix(const SMatrix & m);
  ~SMatrix();

  void setN(unsigned N);
  void append(const std::vector<int> & column);

  bool filter(std::vector<BigNumber> & smooth, std::vector<BigNumber> & acoeff, std::vector<int> & fb);
  void rm_rc(int row, int column);

  // Get methods
  unsigned getN() const;
  unsigned getM() const;
  GFElem get(int row, int column) const;
  GFElem zero() const;
  std::vector<GFElem> getRow(unsigned n) const;
  std::vector<GFElem> getColumn(unsigned m) const;

  // Set methods
  void set(int row, int column, const GFElem & value);
  void set(int N, int M, const GFElem & zero,
	  const std::vector<GFElem> & v, 
	  const std::vector<unsigned> & r, 
	  const std::vector<unsigned> & c);
  void set(const SMatrix & m);
  // Ops
  SMatrix transpose() const;
  // Arithmetic operators
  SMatrix & operator*=(const GFElem & b);
  friend SMatrix operator+(const SMatrix & a, const SMatrix & b);
  void add_col(unsigned col1, unsigned col2); // adds col2 to col1
  friend SMatrix operator*(const GFElem & lhs, const SMatrix & rhs);
  friend SMatrix operator*(const SMatrix & a, const SMatrix & b);
  friend std::vector<GFElem> operator*(const std::vector<GFElem> & a, const SMatrix & b);
  friend std::vector<GFElem> operator*(const SMatrix & a, const std::vector<GFElem> & b);
  friend SMatrix mulTranspose(const SMatrix & A);

  // Arithmetic vector ops
  static std::vector<GFElem> emulv(const GFElem & lhs, const std::vector<GFElem> & rhs);
  static std::vector<GFElem> & add(std::vector<GFElem> & acc, const std::vector<GFElem> & b);
  static std::vector<GFElem> & add(const std::vector<GFElem> & a, const std::vector<GFElem> & b, std::vector<GFElem> & result);
  static std::vector<GFElem> & sub(std::vector<GFElem> & acc, const std::vector<GFElem> & b);
  static std::vector<GFElem> & sub(const std::vector<GFElem> & a, const std::vector<GFElem> & b, std::vector<GFElem> & result);
  static bool equals(const std::vector<GFElem> & a, const std::vector<GFElem> & b);
  static std::vector<GFElem> gen_random_GF_vector(Gen & gen, const GF & field, unsigned n);
  static unsigned weight(const GFElem & zero, const std::vector<GFElem> & vec);
  // Dot product
  static void dprod(const std::vector<GFElem> & a, const std::vector<GFElem> & b, GFElem & res);
  static unsigned getHash(const std::vector<GFElem> & vec);

  static std::vector<GFElem> cg_method(const SMatrix & A, const std::vector<GFElem> & b, 
				       const std::vector<GFElem> & x0, 
				       const SMatrix & At, const std::vector<GFElem> & AS,
				       int & flag, int & scount);
  static std::vector<GFElem> solve_sparse(const SMatrix & A, const std::vector<GFElem> & b, Gen & gen);
  // Tests
  bool empty() const;
  // Output
  void printData() const;
  void printMatrix() const;
 private:
  // нулевой элемент
  GFElem m_zero;
  // размерность матрицы.
  unsigned m_N;
  unsigned m_M;
  // содержит элементы матрицы.
  std::vector<GFElem> m_v;
  // содержит номера позиций m_v, указывающие на первый ненулевой элемент каждой строки.
  std::vector<unsigned> m_r;
  // содержит номера столбцов каждого элемента.
  std::vector<unsigned> m_c;
};

#endif /* _SPARSE_H_ */
