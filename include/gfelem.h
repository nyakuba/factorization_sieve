#ifndef _GFELEM_H_
#define _GFELEM_H_

#include <vector>
#include <stdexcept>
#include <iostream>
#include "gf.h"

namespace test_gf {
  struct testing;
}

// Класс, реализующий вычисления в поле GF(2^m).
// Элементы поля хранятся в виде многочленов.
// Максимальная степень многочлена - 32 (unsigned).
// Таким образом, m должно принадлежать [1, 32].
// При умножении двух многочленов max deg = 32+32 = 64.
// Поэтому значения хранятся в long, чтобы избежать 
//   многократных приведений по модулю.
class GFElem {
 public:
  GFElem();
  GFElem(const GF & field);
  GFElem(const GF & field, const unsigned & value);
  GFElem(const GF & field, const std::vector<int> & coeffs);
  GFElem(const GFElem & elem);
  ~GFElem();

  const GF * getGF() const;
  unsigned getValue() const;

  void Set(const unsigned & value);
  void Set(const std::vector<int> & coeffs);

  GFElem & operator =(const GFElem & a);

  GFElem & operator +=(const GFElem & a);
  GFElem & operator -=(const GFElem & a);
  GFElem & operator *=(const GFElem & a);
  GFElem & operator /=(const GFElem & a);

  friend GFElem operator +(const GFElem & a, const GFElem & b);
  friend GFElem operator -(const GFElem & a, const GFElem & b);
  friend GFElem operator *(const GFElem & a, const GFElem & b);
  friend GFElem operator /(const GFElem & a, const GFElem & b);

  GFElem & operator >>=(const unsigned & a);
  GFElem & operator <<=(const unsigned & a);

  friend GFElem operator >>(const GFElem & a, const unsigned & value);
  friend GFElem operator <<(const GFElem & a, const unsigned & value);
  
  GFElem inverseAdd() const;
  GFElem inverseMul() const;

  friend bool operator ==(const GFElem & a, const GFElem & b);
  friend bool operator !=(const GFElem & a, const GFElem & b);
  
  operator unsigned() const;

  GFElem subst_one() const;
  GFElem getCoeff(int n) const;

 private:
  void mod();
  static unsigned mul_poly(const unsigned & a, const unsigned & b);
  static void div_poly(const unsigned & a, const unsigned & b, unsigned & q, unsigned & r);
  static void euclid(const unsigned & a, const unsigned & b, unsigned & u, unsigned & v, unsigned & gcd);

  const GF * m_field;
  unsigned long m_coeffs;

  // for testing
  friend struct test_gf::testing;
};

#endif /* _GFELEM_H_ */
