#include "include/gf.h"

GF::GF(const unsigned & m, const unsigned & prim):
  m_m(m), m_primitive(prim) {}

GF::GF(const unsigned & m, const std::vector<int> & prim_coeffs):
  m_m(m), m_primitive(0) {
  for(unsigned i = 0; i < prim_coeffs.size(); ++i){
    if (prim_coeffs[i] != 0)
      ++m_primitive;
    m_primitive <<= 1;
  }
  m_primitive >>= 1;
}

GF::~GF() {}

unsigned GF::getM() const {
  return m_m;
}

unsigned GF::getPrimitive() const {
  return m_primitive;
}

bool operator ==(const GF & a, const GF & b){
  return a.m_primitive == b.m_primitive && a.m_m == b.m_m;
}
bool operator !=(const GF & a, const GF & b){
  return !(a == b);
}
