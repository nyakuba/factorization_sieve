COMPILER = icc
CFLAGS = -W \
         -Wall \
         -std=c++11 \
	 -pthread
MAIN = main.cpp
DEPS = bignum.cpp \
       eratosthenes.cpp \
       sieve.cpp \
       gf.cpp \
       gfelem.cpp \
       sparse.cpp \
       rand.cpp
INCLUDE_PATH = -I/opt/intel/ipp/include \
               -I./include \
	       -I.
LIBS_PATH = -L/opt/intel/ipp/lib \
            -L/opt/intel/ipp/lib/intel64
LIBS = -lippcp \
       -lippcore \
       -lm

OPTIMIZATION_OPTS = -axSSE4.2 \
                    -fast \
		    -xHost \
		    -use-intel-optimized-headers \
		    -parallel
# AVX, SSE4.1, SSE3, SSE2 ... -axcode

OUTPUT = quadr_sieve_factor

TEST_DEPS = test/bn_test \
	    test/eratosthene_test \
	    test/sieve_test \
	    test/gf_test \
	    test/sparse_test \
	    test/genbn_test
TEST_LIBS = -lboost_unit_test_framework
TEST_OPTS = --log_level=test_suite

debug: $(DEPS)
	$(COMPILER) -g $(CFLAGS) $(MAIN) $(DEPS) $(INCLUDE_PATH) $(LIBS_PATH) $(LIBS) -o $(OUTPUT)

optimized: $(DEPS)
	$(COMPILER) $(OPTIMIZATION_OPTS) $(CFLAGS) $(MAIN) $(DEPS) $(INCLUDE_PATH) $(LIBS_PATH) $(LIBS) -o $(OUTPUT)

release: $(DEPS)
	$(COMPILER) $(CFLAGS) $(MAIN) $(DEPS) $(INCLUDE_PATH) $(LIBS_PATH) $(LIBS) -o $(OUTPUT)

tests: compile-tests
	for test in $(TEST_DEPS); do \
		$$test $(TEST_OPTS); \
	done

compile-tests: 
	for test in $(TEST_DEPS); do \
		$(COMPILER) -g $(CFLAGS) $$test.cpp $(DEPS) $(INCLUDE_PATH) $(LIBS_PATH) $(LIBS) $(TEST_LIBS) -o $$test; \
	done

clean: clean-main clean-tests

clean-main:
	rm -f $(OUTPUT)

clean-tests:
	rm -f $(TEST_DEPS)
