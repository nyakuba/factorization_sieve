#include "include/gfelem.h"

GFElem::GFElem():
  m_field(nullptr), m_coeffs(0) {}

GFElem::GFElem(const GF & field):
  m_field(&field), m_coeffs(0) {}

GFElem::GFElem(const GF & field, const unsigned & value):
  m_field(&field), m_coeffs(value) {
  mod();
}

GFElem::GFElem(const GF & field, const std::vector<int> & coeffs):
  m_field(&field), m_coeffs(0) {
  Set(coeffs);
}

GFElem::GFElem(const GFElem & elem):
  m_field(elem.m_field), m_coeffs(elem.m_coeffs) {}

GFElem::~GFElem() {}

const GF * GFElem::getGF() const {
  return m_field;
}

unsigned GFElem::getValue() const {
  return m_coeffs;
}

void GFElem::Set(const unsigned & value) {
  m_coeffs = value;
  mod();
}

void GFElem::Set(const std::vector<int> & coeffs) {
  m_coeffs = 0;
  for (unsigned i = 0; i < coeffs.size(); ++i) {
    if (coeffs[i] != 0)
      ++m_coeffs;
    m_coeffs <<= 1;
  }
  m_coeffs >>= 1;
  mod();
}

GFElem & GFElem::operator =(const GFElem & a) {
  if (this != &a) {
    m_field = a.m_field;
    m_coeffs = a.m_coeffs;
  }
  return *this;
}

GFElem & GFElem::operator +=(const GFElem & a) {
  if (m_field != a.m_field && *m_field != *a.m_field)
    throw std::invalid_argument("GF+=: different fields");
  this->m_coeffs ^= a.m_coeffs;
  return *this;
}

GFElem & GFElem::operator -=(const GFElem & a) {
  if (m_field != a.m_field && *m_field != *a.m_field)
    throw std::invalid_argument("GF-=: different fields");
  this->m_coeffs ^= a.m_coeffs;
  return *this;
}

GFElem & GFElem::operator *=(const GFElem & a) {
  if (m_field != a.m_field && *m_field != *a.m_field)
    throw std::invalid_argument("GF*=: different fields");
  *this = *this * a;
  return *this;
}

GFElem & GFElem::operator /=(const GFElem & a) {
  if (m_field != a.m_field && *m_field != *a.m_field)
    throw std::invalid_argument("GF/=: different fields");
  *this *= a.inverseMul();
  return *this;
}

GFElem operator +(const GFElem & a, const GFElem & b) {
  if (a.m_field != b.m_field && *a.m_field != *b.m_field)
    throw std::invalid_argument("GF+: different fields");
  return GFElem(*a.m_field, a.m_coeffs ^ b.m_coeffs);
}
 
GFElem operator -(const GFElem & a, const GFElem & b) {
  if (a.m_field != b.m_field && *a.m_field != *b.m_field)
    throw std::invalid_argument("GF-: different fields");
  return GFElem(*a.m_field, a.m_coeffs ^ b.m_coeffs);
}

GFElem operator *(const GFElem & a, const GFElem & b) {
  // std::cout << "mul" << std::endl;
  if (a.m_field != b.m_field && *a.m_field != *b.m_field)
    throw std::invalid_argument("GF*: different fields");
  GFElem lhs(a);
  unsigned rhs = b.m_coeffs;
  GFElem accum(*a.m_field); 
  while (rhs != 0) {
    // std::cout << ": acc = " << accum.m_coeffs << ", lhs = " << lhs.m_coeffs << ", rhs = " << rhs  << std::endl;
    if (rhs % 2 == 1) {
      accum += lhs;
    }
    lhs <<= 1;
    rhs >>= 1;
  }
  return GFElem(*a.m_field, accum);
}

GFElem operator /(const GFElem & a, const GFElem & b) {
  if (a.m_field != b.m_field && *a.m_field != *b.m_field)
    throw std::invalid_argument("GF/: different fields");
  return a * b.inverseMul();
}

GFElem & GFElem::operator >>=(const unsigned & a) {
  m_coeffs >>= a;
  mod();
  return *this;
}

GFElem & GFElem::operator <<=(const unsigned & a) {
  m_coeffs <<= a;
  mod();
  return *this;
}

GFElem operator >>(const GFElem & a, const unsigned & value) {
  return GFElem(*a.m_field, a.m_coeffs >> value);
}

GFElem operator <<(const GFElem & a, const unsigned & value) {
  return GFElem(*a.m_field, a.m_coeffs << value);
}
  
GFElem GFElem::inverseAdd() const {
  return *this;
}

GFElem GFElem::inverseMul() const {
  if (m_coeffs == 0)
    throw std::invalid_argument("GF: inverseMul(zero) undefined.");
  unsigned inverse = 0, unused = 0, unused1 = 0;
  euclid(this->m_coeffs, this->m_field->getPrimitive(), inverse, unused, unused1);
  return GFElem(*m_field, inverse);
}

bool operator ==(const GFElem & a, const GFElem & b) {
  if (a.m_field != b.m_field && *a.m_field != *b.m_field) {
    if (a.m_field->getM() != b.m_field->getM())
      return false;
    if (a.m_field->getPrimitive() != b.m_field->getPrimitive())
      return false;
  }
  return a.m_coeffs == b.m_coeffs;
}

bool operator !=(const GFElem & a, const GFElem & b) {
  return !(a == b);
}

GFElem::operator unsigned() const {
  return m_coeffs;
}

GFElem GFElem::subst_one() const {
  unsigned c = m_coeffs;
  unsigned w = 0;
  while (c != 0) {
    c &= c-1;
    ++w;
  }
  return GFElem(*m_field, w%2);
}

GFElem GFElem::getCoeff(int n) const {
  return GFElem(*m_field, (m_coeffs >> n) % 2);
}

void GFElem::mod() {
  unsigned am = 1 << m_field->getM();
  if (m_coeffs >= am) {
    // std::cout << "mod" << std::endl;
    // младшие коэффициенты сохраняем.
    unsigned lcoeffs = m_coeffs & ~(-1U << m_field->getM());
    // работаем только со старшими коэффициентами.
    unsigned hcoeffs = m_coeffs >> m_field->getM();
    unsigned correction = am ^ m_field->getPrimitive();
    unsigned add_value = correction;
    while (hcoeffs != 0) {
      if (hcoeffs % 2 == 1) {
	lcoeffs ^= add_value;
      }
      add_value <<= 1;
      // проверяем, нужна ли коррекция
      if (add_value >= am) {
	add_value ^= am;
	add_value ^= correction;
      }
      hcoeffs >>= 1;
    }
    m_coeffs = lcoeffs;
  }
}

// Умножение двух многочленов в поле GF(2).
unsigned GFElem::mul_poly(const unsigned & a, const unsigned & b) {
  // std::cout << "mul" << std::endl;
  unsigned lhs = a;
  unsigned rhs = b;
  unsigned accum = 0; 
  while (rhs != 0) {
    if (rhs % 2 == 1) {
      accum ^= lhs;
    }
    lhs <<= 1;
    rhs >>= 1;
  }
  return accum;
}

// Деление двух многочленов в поле GF(2).
void GFElem::div_poly(const unsigned & a, const unsigned & b, 
		      unsigned & q, unsigned & r) {
  if (b == 0)
    throw std::invalid_argument("div_poly: division by zero");
  // std::cout << "div: a " << a << ", b " << b << std::endl;
  r = a; // остаток
  q = 0; // результат
  unsigned b_bitsize = 0;
  unsigned tmp = b;
  while (tmp != 0) {
    tmp >>= 1;
    ++b_bitsize;
  }
  --b_bitsize;
  while (r >= b) {
    unsigned pdiv = r;
    unsigned r_bitsize = 0;
    while (pdiv != 0) {
      pdiv >>= 1;
      ++r_bitsize;
    }
    --r_bitsize;
    r ^= b << (r_bitsize - b_bitsize);
    q ^= 1 << (r_bitsize - b_bitsize);
  }
}

void GFElem::euclid(const unsigned & a, const unsigned & b, 
		    unsigned & u, unsigned & v, unsigned & gcd) {
  if (b == 0)
    throw std::invalid_argument("div_poly: division by zero");
  unsigned g = a, h = b;
  unsigned s1 = 0, s2 = 1, t1 = 1, t2 = 0;
  unsigned s = 0, t = 0, q = 0, r = 0;
  while (h != 0) {
    // std::cout << h << std::endl;
    div_poly(g, h, q, r);
    s = s2 ^ mul_poly(q, s1);
    t = t2 ^ mul_poly(q, t1);
    g = h; h = r;
    s2 = s1; s1 = s;
    t2 = t1; t1 = t;
  }
  gcd = g;
  u = s2;
  v = t2;
}
