#include "include/bignum.h"
//////////////////////////////////////////////////////////////////////
//
// BigNumber
//
//////////////////////////////////////////////////////////////////////
BigNumber::~BigNumber() {
  delete [] (Ipp8u*)m_pBN;
}

bool BigNumber::create(const Ipp32u* pData, int length, IppsBigNumSGN sgn) {
  int size;
  // TODO replace ippsBigNumGetSize.
  ippsBigNumGetSize(length, &size);
  m_pBN = (IppsBigNumState*)( new Ipp8u[size] );
  if(!m_pBN)
    return false;
  ippsBigNumInit(length, m_pBN);
  if(pData)
    ippsSet_BN(sgn, length, pData, m_pBN);
  return true;
}

// constructors
//
BigNumber::BigNumber(Ipp32u value) {
  create(&value, 1, IppsBigNumPOS);
}

BigNumber::BigNumber(Ipp32s value) {
  Ipp32s avalue = abs(value);
  create((Ipp32u*)(&avalue), 1, (value<0)? IppsBigNumNEG :
	 IppsBigNumPOS);
}

BigNumber::BigNumber(const IppsBigNumState* pBN) {
  IppsBigNumSGN sgn;
  int bnLen;
  Ipp32u * bnData;
  ippsRef_BN(nullptr, &bnLen, nullptr, pBN);
  bnLen /= 8*sizeof(Ipp32u);
  ++bnLen;
  //
  create(bnData, bnLen, sgn);
  //
}

BigNumber::BigNumber(const Ipp32u* pData, int length, IppsBigNumSGN sgn) {
  create(pData, length, sgn);
}

static char HexDigitList[] = "0123456789ABCDEF";

BigNumber::BigNumber(const char* s) {
  bool neg = '-' == s[0];
  if(neg) s++;
  bool hex = ('0'==s[0]) && (('x'==s[1]) || ('X'==s[1]));
  int dataLen;
  Ipp32u base;
  if(hex) {
    s += 2;
    base = 0x10;
    dataLen = (strlen(s) + 7)/8;
  }
  else {
    base = 10;
    dataLen = (strlen(s) + 9)/10;
  }
  create(0, dataLen);
  *(this) = Zero();
  while(*s) {
    char tmp[2] = {s[0],0};
    Ipp32u digit = strcspn(HexDigitList, tmp);
    *this = (*this) * base + BigNumber( digit );
    s++;
  }
  if(neg)
    (*this) = Zero()- (*this);
}

BigNumber::BigNumber(const BigNumber& bn) {
  IppsBigNumSGN sgn;
  int length;
  Ipp32u * pData;
  ippsRef_BN(&sgn, &length, &pData, bn.m_pBN);
  length /= 8*sizeof(Ipp32u);
  ++length;
  //
  create(pData, length, sgn);
  //
}

// set value
//
void BigNumber::Set(const Ipp32u* pData, int length, IppsBigNumSGN sgn) {
  ippsSet_BN(sgn, length, pData, BN(*this));
}

// constants
//
const BigNumber& BigNumber::Zero() {
  static const BigNumber zero(0);
  return zero;
}
const BigNumber& BigNumber::One() {
  static const BigNumber one(1);
  return one;
}
const BigNumber& BigNumber::Two() {
  static const BigNumber two(2);
  return two;
}

// arithmetic operators
//
BigNumber& BigNumber::operator = (const BigNumber& bn) {
  if(this != &bn) {
    // prevent self copy
    IppsBigNumSGN sgn;
    int length;
    Ipp32u* pData;
    ippsRef_BN(&sgn, &length, &pData, bn.m_pBN);
    length /= 8*sizeof(Ipp32u);
    ++length;
    //
    delete (Ipp8u*)m_pBN;
    create(pData, length, sgn);
    //
  }
  return *this;
}

BigNumber& BigNumber::operator += (const BigNumber& bn) {
  int aLength;
  ippsRef_BN(nullptr, &aLength, nullptr, BN(*this));
  aLength /= 8*sizeof(Ipp32u);
  ++aLength;
  int bLength;
  ippsRef_BN(nullptr, &bLength, nullptr, BN(bn));
  bLength /= 8*sizeof(Ipp32u);
  ++bLength;
  int rLength = IPP_MAX(aLength,bLength) + 1;
  BigNumber result(0, rLength);
  ippsAdd_BN(BN(*this), BN(bn), BN(result));
  *this = result;
  return *this;
}

BigNumber& BigNumber::operator -= (const BigNumber& bn) {
  int aLength;
  ippsRef_BN(nullptr, &aLength, nullptr, BN(*this));
  aLength /= 8*sizeof(Ipp32u);
  ++aLength;
  int bLength;
  ippsRef_BN(nullptr, &bLength, nullptr, BN(bn));
  bLength /= 8*sizeof(Ipp32u);
  ++bLength;
  int rLength = IPP_MAX(aLength,bLength);
  BigNumber result(0, rLength);
  ippsSub_BN(BN(*this), BN(bn), BN(result));
  *this = result;
  return *this;
}

BigNumber& BigNumber::operator *= (const BigNumber& bn) {
  int aLength;
  ippsRef_BN(nullptr, &aLength, nullptr, BN(*this));
  aLength /= 8*sizeof(Ipp32u);
  ++aLength;
  int bLength;
  ippsRef_BN(nullptr, &bLength, nullptr, BN(bn));
  bLength /= 8*sizeof(Ipp32u);
  ++bLength;
  int rLength = aLength+bLength;
  BigNumber result(0, rLength);
  ippsMul_BN(BN(*this), BN(bn), BN(result));
  *this = result;
  return *this;
}

BigNumber& BigNumber::operator *= (Ipp32u n) {
  int aLength;
  ippsRef_BN(nullptr, &aLength, nullptr, BN(*this));
  aLength /= 8*sizeof(Ipp32u);
  ++aLength;
  BigNumber bn(n);
  BigNumber result(0, aLength+1);
  ippsMul_BN(BN(*this), BN(bn), BN(result));
  *this = result;
  return *this;
}

BigNumber& BigNumber::operator %= (const BigNumber& bn) {
  BigNumber remainder(bn);
  ippsMod_BN(BN(*this), BN(bn), BN(remainder));
  *this = remainder;
  return *this;
}

BigNumber& BigNumber::operator /= (const BigNumber& bn) {
  BigNumber quotient(*this);
  BigNumber remainder(bn);
  ippsDiv_BN(BN(*this), BN(bn), BN(quotient), BN(remainder));
  *this = quotient;
  return *this;
}

BigNumber operator + (const BigNumber& a, const BigNumber& b ) {
  BigNumber r(a);
  return r += b;
}

BigNumber operator - (const BigNumber& a, const BigNumber& b ) {
  BigNumber r(a);
  return r -= b;
}

BigNumber operator * (const BigNumber& a, const BigNumber& b ) {
  BigNumber r(a);
  return r *= b;
}

BigNumber operator * (const BigNumber& a, Ipp32u n) {
  BigNumber r(a);
  return r *= n;
}

BigNumber operator / (const BigNumber& a, const BigNumber& b ) {
  BigNumber q(a);
  return q /= b;
}

BigNumber operator % (const BigNumber& a, const BigNumber& b ) {
  BigNumber r(b);
  ippsMod_BN(BN(a), BN(b), BN(r));
  return r;
}

void BigNumber::babs(){
  IppsBigNumSGN psgn;
  int bitsize;
  Ipp32u * data;
  ippsRef_BN(&psgn, &bitsize, &data, m_pBN);
  psgn = IppsBigNumPOS;
  ippsSet_BN(psgn, bitsize/sizeof(Ipp32u) + 1, data, m_pBN);
}

void BigNumber::bpow(const BigNumber& power) {
  if (power == BigNumber::Zero()){
    if (*this == BigNumber::Zero())
      throw std::invalid_argument("bpow : Zero^(Zero) undefined.");
    *this = BigNumber::One();
  } else {
    BigNumber k(*this);
    for (BigNumber i(1U); i < power; i+=1) {
      *this *= k;
    }
  }
}

void BigNumber::bpow(unsigned pow) {
  if (pow == 0){
    if (*this == BigNumber::Zero())
      throw std::invalid_argument("bpow : Zero^(Zero) undefined.");
    *this = BigNumber::One();
  } else {
    BigNumber k(*this);
    for (unsigned i = 1; i < pow; ++i) {
      *this *= k;
    }
  }
}

void BigNumber::bsqrt() {
  IppsBigNumSGN sgn;
  int bitsize;
  ippsRef_BN(&sgn, &bitsize, nullptr, m_pBN);
  if (sgn == IppsBigNumNEG)
    throw std::invalid_argument("sqrt: a can not be negative.");

  unsigned slen = bitsize/(sizeof(Ipp32u)*8) + 1;
  Ipp32u * sdata = new Ipp32u[slen];
  for (unsigned i = 0; i < slen; ++i)
    sdata[i] = 0;
  sdata[slen-1] = 1U;
  sdata[slen-1] <<= bitsize - (slen - 1)*sizeof(Ipp32u)*8;
  BigNumber x1(sdata, slen);
  BigNumber x2((x1 + *this / x1) / BigNumber::Two());
    // std::cout << "sqrt start:x1 = " << x1 << ", x2 = " << x2 << std::endl;
  while (x2 < x1) {
    x1 = x2;
    x2 = (x1 + *this / x1) / BigNumber::Two();
    // std::cout << "sqrt:x1 = " << x1 << ", x2 = " << x2 << std::endl;
  }
  *this = x1;
  delete [] sdata;
}

// ln evaluation
double loge(const BigNumber & a) {
  if (a <= BigNumber::Zero()) 
    throw std::invalid_argument("loge: arg must be positive.");
  IppsBigNumSGN sgn;
  int bitsize = 0;
  Ipp32u * data = nullptr;
  ippsRef_BN(&sgn, &bitsize, &data, a.m_pBN);
  int hindex = bitsize / (8 * sizeof(Ipp32u));
  if (hindex < 1)
    return log(static_cast<double>(data[0]));

  Ipp32u h = data[hindex];
  Ipp32u l = data[hindex-1];
  int hsize = 8 * sizeof(Ipp32u) * (hindex+1) - bitsize;
  h <<= hsize;
  l >>= (8 * sizeof(Ipp32u) - hsize);
  h += l;
  return 0.693147181 * (bitsize - 8*sizeof(Ipp32u)) + log(static_cast<double>(h));
}

// modulo arithmetic
//
BigNumber BigNumber::Modulo(const BigNumber& a) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("Modulo: mod must be > 1.");
  return a % *this;
}
BigNumber BigNumber::InverseAdd(const BigNumber& a) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("InverseAdd: mod must be > 1.");
  BigNumber t = Modulo(a);
  if(t==BigNumber::Zero())
    return t;
  else
    return *this - t;
}
BigNumber BigNumber::InverseMul(const BigNumber& a) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("InverseMul: mod must be > 1.");
  if (a == BigNumber::Zero())
    throw std::invalid_argument("InverseMul: zero inverse is undefined.");
  BigNumber r(*this);
  BigNumber a1(this->Modulo(a));
  ippsModInv_BN(BN(a1), BN(*this), BN(r));
  return r;
}
BigNumber BigNumber::ModAdd(const BigNumber& a, const BigNumber& b) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("ModAdd: mod must be > 1.");
  BigNumber r = this->Modulo(a+b);
  return r;
}
BigNumber BigNumber::ModSub(const BigNumber& a, const BigNumber& b) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("ModSub: mod must be > 1.");
  BigNumber r = this->Modulo(a + this->InverseAdd(b));
  return r;
}
BigNumber BigNumber::ModMul(const BigNumber& a, const BigNumber& b) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("ModMul: mod must be > 1.");
  BigNumber r = this->Modulo(a*b);
  return r;
}

BigNumber BigNumber::ModPow1(const BigNumber&a, const BigNumber& k) const {
	int deg = k.BitSize();
	BigNumber b(BigNumber::One());
	if (k == BigNumber::Zero())
		return b;

	std::string s;
	k.num2hex(s);
	Ipp32u size = 0;
	Ipp32u pl = 3;
	while (s[pl] == '0')
		++pl;
	size = s.length() - pl;
	std::vector<std::string> bit(size);
	for (auto& i : bit){
		switch (s[pl]){
		case '0': i = "0000"; break; case '4': i = "0100"; break; case '8': i = "1000"; break; case 'C': i = "1100"; break;
		case '1': i = "0001"; break; case '5': i = "0101"; break; case '9': i = "1001"; break; case 'D': i = "1101"; break;
		case '2': i = "0010"; break; case '6': i = "0110"; break; case 'A': i = "1010"; break; case 'E': i = "1110"; break;
		case '3': i = "0011"; break; case '7': i = "0111"; break; case 'B': i = "1011"; break; case 'F': i = "1111"; break;
		}
		++pl;
	}
	//Now, we have all bits of k
	BigNumber F(a);
	if (bit[size - 1][3] == '1')
		b = a;
	for (int i = 1; i < deg; i++){
		F *= F;
		F %= *this;
		if (bit[size - 1 - i / 4][3 - i % 4] == '1'){
			b *= F;
			b %= *this;
		}
	}
	return b;
}


bool BigNumber::isDivTwo() const {
  Ipp32u * data;
  ippsRef_BN(nullptr, nullptr, &data, m_pBN);
  return (data[0] & 1) == 0;
}

BigNumber BigNumber::ModPow(const BigNumber& a, const BigNumber& power) const {
  if (*this <= BigNumber::One())
    throw std::invalid_argument("ModPow: mod must be > 1.");
  if (a == BigNumber::Zero() && power == BigNumber::Zero())
    throw std::invalid_argument("ModPow: Zero^(Zero) undefined.");

  int size = 0;  
  int bn_bitsize = 0;
  Ipp32u * data = nullptr;
  ippsRef_BN(nullptr, &bn_bitsize, &data, this->m_pBN);
  // Модуль должен быть нечетным числом.
  if ((data[0] & 1) == 1)
    return this->ModPow1(a, power);
  int bn_size = std::ceil(((double) bn_bitsize)/(sizeof(Ipp32u)*8));

  ippsBigNumGetSize(bn_size, &size);
  IppsBigNumState * _a = (IppsBigNumState *)(new Ipp8u [size]);
  ippsBigNumInit(bn_size, _a);
  ippsMod_BN(BN(a), BN(*this), _a);

  ippsBigNumGetSize(bn_size, &size);
  IppsBigNumState * _power = (IppsBigNumState *)(new Ipp8u [size]);
  ippsBigNumInit(bn_size, _power);
  ippsMod_BN(BN(power), BN(BigNumber(*this - BigNumber::One())), _power);

  ippsMontGetSize(IppsBinaryMethod, bn_size, &size);

  IppsMontState * pMont = (IppsMontState *)(new Ipp8u [size]);
  ippsMontInit(IppsBinaryMethod, bn_size, pMont);
  ippsMontSet(data, bn_size, pMont);

  ippsBigNumGetSize(bn_size, &size);
  IppsBigNumState * mont_a = (IppsBigNumState *)(new Ipp8u [size]);
  ippsBigNumInit(bn_size, mont_a);
  ippsMontForm(_a, pMont, mont_a);
  
  Ipp32u * pdata = new Ipp32u[bn_size];
  BigNumber ret(pdata, bn_size);

  ippsMontExp(mont_a, _power, pMont, BN(ret));
  
  BigNumber one(1);
  ippsMontMul(BN(ret), BN(one), pMont, BN(ret));
  
  delete [] (Ipp8u *) pMont;
  delete [] (Ipp8u *) mont_a;
  delete [] (Ipp8u *) _a;
  delete [] (Ipp8u *) _power;

  return ret;
}

BigNumber BigNumber::legendre_symbol(const BigNumber& a) const {
  return this->ModPow(a, (*this - BigNumber::One())/2);
}

void bgcd(const BigNumber & a, const BigNumber & b, BigNumber & gcd) {
    ippsGcd_BN(a.m_pBN, b.m_pBN, gcd.m_pBN);
}


// comparison
//
// int BigNumber::compare(const BigNumber &bn) const {
//   Ipp32u result;
//   BigNumber tmp = *this - bn;
//   ippsCmpZero_BN(BN(tmp), &result);
//   return (result==IS_ZERO)? 0 : (result==GREATER_THAN_ZERO)? 1
//     : -1;
// }
IppStatus BigNumber::compare(const IppsBigNumState * pBN, Ipp32u * result) const {
  return ippsCmp_BN(pBN, this->m_pBN, result);
}
bool BigNumber::operator < (const BigNumber &b) const { 
  Ipp32u result;
  b.compare(this->m_pBN, &result); 
  return result == LESS_THAN_ZERO;
}
bool BigNumber::operator > (const BigNumber &b) const { 
  Ipp32u result;
  b.compare(this->m_pBN, &result); 
  return result == GREATER_THAN_ZERO;
}
bool BigNumber::operator == (const BigNumber & b) const {
  Ipp32u result;
  b.compare(this->m_pBN, &result); 
  return result == IS_ZERO;
}
bool BigNumber::operator != (const BigNumber &b) const { 
  return !(*this == b);
}

// easy tests
//
bool BigNumber::IsOdd() const {
  std::vector<Ipp32u> v;
  num2vec(v);
  return v[0]&1;
}

// size of BigNumber
//
int BigNumber::LSB() const {
  if( *this == BigNumber::Zero() )
    return 0;
  std::vector<Ipp32u> v;
  num2vec(v);
  int lsb = 0;
  std::vector<Ipp32u>::iterator i;
  for(i=v.begin(); i!=v.end(); i++) {
    Ipp32u x = *i;
    if(0==x)
      lsb += 32;
    else {
      while(0==(x&1)) {
	lsb++;
	x >>= 1;
      }
      break;
    }
  }
  return lsb;
}

int BigNumber::MSB() const {
  if( *this == BigNumber::Zero() )
    return 0;
  std::vector<Ipp32u> v;
  num2vec(v);
  int msb = v.size()*32 -1;
  std::vector<Ipp32u>::reverse_iterator i;
  for(i=v.rbegin(); i!=v.rend(); i++) {
    Ipp32u x = *i;
    if(0==x)
      msb -=32;
    else {
      while(!(x&0x80000000)) {
	msb--;
	x <<= 1;
      }
      break;
    }
  }
  return msb;
}

int Bit(const std::vector<Ipp32u>& v, int n) {
  return 0 != ( v[n>>5] & (1<<(n&0x1F)) );
}

unsigned BigNumber::uns() const {
  std::string hexstr;
  num2hex(hexstr);
  // std::cout << hexstr << std::endl;
  unsigned ret = 0;
  unsigned base = 1;
  for (unsigned i = hexstr.size()-1; hexstr[i] != 'x'; --i) {
    if (hexstr[i] != '0') {
      char ch = hexstr[i];
      if (ch <= '9') {
	ret += (ch - '0')*base;
      } else {
	ret += (ch - 'A' + 10)*base;
      }
    }
    base *= 16;
  }
  // std::cout << ret << std::endl;
  return ret;
}

// conversions and output
//
void BigNumber::num2vec( std::vector<Ipp32u>& v ) const {
  IppsBigNumSGN sgn;
  int length;
  Ipp32u* pData;
  ippsRef_BN(&sgn, &length, &pData, BN(*this));
  length /= 8*sizeof(Ipp32u);
  ++length;
  //
  for(int n=0; n<length; n++)
    v.push_back( pData[n] );
  //
}

void BigNumber::num2hex( std::string& s ) const {
  IppsBigNumSGN sgn;
  int length;
  Ipp32u* pData;
  ippsRef_BN(&sgn, &length, &pData, BN(*this));
  length /= 8*sizeof(Ipp32u);
  ++length;
  s.append(1, (sgn==IppsBigNumNEG)? '-' : ' ');
  s.append(1, '0');
  s.append(1, 'x');
  for(int n=length; n>0; n--) {
    Ipp32u x = pData[n-1];
    for(int nd=8; nd>0; nd--) {
      char c = HexDigitList[(x>>(nd-1)*4)&0xF];
      s.append(1, c);
    }
  }
}

std::ostream& operator << (std::ostream &os, const BigNumber& a) {
  std::string s;
  a.num2hex(s);
  os << s.c_str();
  return os;
}
