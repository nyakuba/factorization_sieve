#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BigNumber_test
#include <boost/test/unit_test.hpp>
#include <ippcp.h>
#include "include/gf.h"
#include "include/gfelem.h"

namespace test_gf {

  struct testing {
    static unsigned mul_poly(const unsigned & a, const unsigned & b) {
      return GFElem::mul_poly(a, b);
    }
    static void div_poly(const unsigned & a, const unsigned & b, unsigned & q, unsigned & r) {
      GFElem::div_poly(a, b, q, r);
    }
    static void euclid(const unsigned & a, const unsigned & b, unsigned & u, unsigned & v, unsigned & gcd) {
      GFElem::euclid(a, b, u, v, gcd);
    }
  };

}

using namespace test_gf;

// General methods
BOOST_AUTO_TEST_SUITE(gf)

BOOST_AUTO_TEST_CASE(creation) {
  unsigned m = 5;
  unsigned prim = 41;
  GF gf(m, prim);
  BOOST_CHECK(gf.getM() == 5);
  BOOST_CHECK(gf.getPrimitive() == 41);

  std::vector<int> coeffs(6);
  coeffs[0] = coeffs[2] = coeffs[5] = 1;
  GF gf1(m, coeffs);
  BOOST_CHECK(gf1.getM() == 5);
  BOOST_CHECK(gf1.getPrimitive() == 41);
}

BOOST_AUTO_TEST_SUITE_END()

// General methods
BOOST_AUTO_TEST_SUITE(gfelem)

BOOST_AUTO_TEST_CASE(creation) {
  unsigned m = 5;
  unsigned prim = 41;
  GF gf(m, prim);

  unsigned value = 10;
  std::vector<int> coeffs(6);
  coeffs[2] = coeffs[4] = 1;

  GFElem el(gf);
  GFElem el1(gf, value);
  GFElem el2(gf, coeffs);

  BOOST_CHECK((unsigned)el == 0);
  BOOST_CHECK((unsigned)el1 == 10);
  BOOST_CHECK((unsigned)el2 == 10);

  BOOST_CHECK(el.getValue() == 0);
  BOOST_CHECK(el1.getValue() == 10);
  BOOST_CHECK(el2.getValue() == 10);

  BOOST_CHECK(el.getGF() == &gf);
  BOOST_CHECK(el1.getGF() == &gf);
  BOOST_CHECK(el2.getGF() == &gf);
}

BOOST_AUTO_TEST_CASE(creation1) {
  unsigned m = 5;
  unsigned prim = 41;
  GF gf(m, prim);

  unsigned value = 32;
  GFElem el(gf, value);
  BOOST_CHECK(el.getValue() == 9);

  unsigned value1 = 41;
  GFElem el1(gf, value1);
  BOOST_CHECK(el1.getValue() == 0);

  unsigned value2 = 64;
  GFElem el2(gf, value2);
  BOOST_CHECK(el2.getValue() == 18);
}

BOOST_AUTO_TEST_CASE(set_methods) {
  unsigned m = 5;
  unsigned prim = 41;
  GF gf(m, prim);

  unsigned value = 10;
  std::vector<int> coeffs(6);
  coeffs[2] = coeffs[5] = 1;

  GFElem el(gf);
  BOOST_CHECK(el.getValue() == 0);
  el.Set(value);
  BOOST_CHECK(el.getValue() == 10);
  el.Set(coeffs);
  BOOST_CHECK(el.getValue() == 9);
}

BOOST_AUTO_TEST_CASE(assignment) {
  GF gf(5, 41);
  GFElem el1(gf);
  BOOST_CHECK((unsigned)el1 == 0);
  GFElem el2(gf, 9);
  el1 = el2;
  BOOST_CHECK((unsigned)el1 == 9);
}

BOOST_AUTO_TEST_SUITE_END()

// Private methods
BOOST_AUTO_TEST_SUITE(gfelem_private_methods)

BOOST_AUTO_TEST_CASE(mul_poly) {
  BOOST_CHECK(testing::mul_poly(5U, 0U) == 0U);
  BOOST_CHECK(testing::mul_poly(0U, 42U) == 0U);
  BOOST_CHECK(testing::mul_poly(33U, 1U) == 33U);
  BOOST_CHECK(testing::mul_poly(1U, 52U) == 52U);
  BOOST_CHECK(testing::mul_poly(20U, 13U) == 228U);
}

BOOST_AUTO_TEST_CASE(div_poly) {
  unsigned a = 25, b = 5, q = 0, r = 0;
  testing::div_poly(a, b, q, r);
  BOOST_CHECK(a == 25);
  BOOST_CHECK(b == 5);
  BOOST_CHECK(q == 7);
  BOOST_CHECK(r == 2);

  BOOST_CHECK_THROW(testing::div_poly(a, 0, q, r), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(euclid) {
  unsigned a = 19, b = 10, u = 0, v = 0, gcd = 0;
  testing::euclid(a, b, u, v, gcd);
  BOOST_CHECK(a == 19);
  BOOST_CHECK(b == 10);
  BOOST_CHECK(u == 7);
  BOOST_CHECK(v == 12);
  BOOST_CHECK(gcd == 1);

  BOOST_CHECK_THROW(testing::euclid(a, 0, u, v, gcd),
		    std::invalid_argument);
}

BOOST_AUTO_TEST_SUITE_END()

// Common arithmetic operations
BOOST_AUTO_TEST_SUITE(gf_ops) 

BOOST_AUTO_TEST_CASE(diff_fields_error) {
  GF gf1(4, 19);
  GF gf2(5, 41);
  GFElem lhs(gf1, 5);
  GFElem rhs(gf2, 10);
  BOOST_CHECK_THROW(lhs + rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs - rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs * rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs / rhs, std::invalid_argument);

  BOOST_CHECK_THROW(lhs += rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs -= rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs *= rhs, std::invalid_argument);
  BOOST_CHECK_THROW(lhs /= rhs, std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(plus_minus) {
  std::vector<int> prim(18);
  prim[0] = prim[11] = prim[17] = 1; // x^18 + x^7 + 1
  GF gf(18, prim);
  {
    GFElem lhs(gf);
    GFElem rhs(gf);
    BOOST_CHECK((unsigned)(lhs + rhs) == 0);
    lhs += rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem lhs(gf);
    GFElem rhs(gf);
    BOOST_CHECK((unsigned)(lhs - rhs) == 0);
    lhs -= rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem el(gf, 7);
    BOOST_CHECK((unsigned)(el + el) == 0);
    el += el;
    BOOST_CHECK((unsigned)el == 0);
  }
  {
    GFElem lhs(gf, 7);
    GFElem rhs(gf, 7);
    BOOST_CHECK((unsigned)(lhs - rhs) == 0);
    lhs -= rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem lhs(gf, 17);
    GFElem rhs(gf, 23);
    BOOST_CHECK((unsigned)(lhs + rhs) == 6);
    lhs += rhs;
    BOOST_CHECK((unsigned)lhs == 6);
  }
  {
    GFElem lhs(gf, 8);
    GFElem rhs(gf, 15);
    BOOST_CHECK((unsigned)(lhs - rhs) == 7);
    lhs -= rhs;
    BOOST_CHECK((unsigned)lhs == 7);
  }
}

BOOST_AUTO_TEST_CASE(mul) {
  unsigned prim = 19;
  GF gf(4, prim);
  {
    GFElem lhs(gf, 0);
    GFElem rhs(gf, 0);
    BOOST_CHECK((unsigned)(lhs * rhs) == 0);
    lhs *= rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem lhs(gf, 121);
    GFElem rhs(gf, 0);
    BOOST_CHECK((unsigned)(lhs * rhs) == 0);
    lhs *= rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem lhs(gf, 8);
    GFElem rhs(gf, 1);
    BOOST_CHECK((unsigned)(lhs * rhs) == 8);
    lhs *= rhs;
    BOOST_CHECK((unsigned)lhs == 8);
  }
  {
    GFElem lhs(gf, 8);
    BOOST_CHECK((unsigned)lhs == 8);
    GFElem rhs(gf, 12);
    BOOST_CHECK_MESSAGE((unsigned)(lhs * rhs) == 10 , "8 * 12: elem = " << (unsigned)(lhs * rhs) << ", expected 10 ");
    lhs *= rhs;
    BOOST_CHECK_MESSAGE((unsigned)lhs == 10 , "8 * 12: elem = " << (unsigned)lhs << ", expected 10 ");
  }
  {
    GFElem lhs(gf, 13);
    GFElem rhs(gf, 13);
    BOOST_CHECK_MESSAGE((unsigned)(lhs * rhs) == 14, "13 * 13: elem = " << (unsigned)(lhs * rhs) << ", expected 14");
    lhs *= rhs;
    BOOST_CHECK_MESSAGE((unsigned)lhs == 14, "13 * 13: elem = " << (unsigned)lhs << ", expected 14");
  }
  {
    GFElem lhs(gf, 3);
    GFElem rhs(gf, 14);
    BOOST_CHECK_MESSAGE((unsigned)(lhs * rhs) == 1, "3 * 14: elem = " << (unsigned)(lhs * rhs) << ", expected 1");
    lhs *= rhs;
    BOOST_CHECK_MESSAGE((unsigned)lhs == 1, "3 * 14: elem = " << (unsigned)lhs << ", expected 1");
  }
  {
    GFElem lhs(gf, 9);
    GFElem rhs(gf, 9);
    BOOST_CHECK_MESSAGE((unsigned)(lhs * rhs) == 13, "9 * 9: elem = " << (unsigned)(lhs * rhs) << ", expected 13");
    lhs *= rhs;
    BOOST_CHECK_MESSAGE((unsigned)lhs == 13, "9 * 9: elem = " << (unsigned)lhs << ", expected 13");
  }
}

BOOST_AUTO_TEST_CASE(div) { 
  GF gf(4, 19);
  {
    GFElem lhs(gf, 13);
    GFElem rhs(gf, 0);
    BOOST_CHECK_THROW(lhs / rhs, std::invalid_argument);
    BOOST_CHECK_THROW(lhs /= rhs, std::invalid_argument);
  }
  {
    GFElem lhs(gf, 13);
    GFElem rhs(gf, 1);
    BOOST_CHECK((unsigned)(lhs / rhs) == 13);
    lhs /= rhs;
    BOOST_CHECK((unsigned)lhs == 13);
  }
  {
    GFElem lhs(gf, 0);
    GFElem rhs(gf, 13);
    BOOST_CHECK((unsigned)(lhs / rhs) == 0);
    lhs /= rhs;
    BOOST_CHECK((unsigned)lhs == 0);
  }
  {
    GFElem lhs(gf, 13);
    GFElem rhs(gf, 13);
    BOOST_CHECK((unsigned)(lhs / rhs) == 1);
    lhs /= rhs;
    BOOST_CHECK((unsigned)lhs == 1);
  }
  {
    GFElem lhs(gf, 13);
    GFElem rhs(gf, 10);
    BOOST_CHECK((unsigned)(lhs / rhs) == 3);
    lhs /= rhs;
    BOOST_CHECK((unsigned)lhs == 3);
  }
}

BOOST_AUTO_TEST_CASE(inverse) {
  GF gf(4, 19);
  GFElem el(gf, 0);
  GFElem el1(gf, 1);
  GFElem el2(gf, 7);
  GFElem el3(gf, 10);

  BOOST_CHECK((unsigned)el.inverseAdd() == 0);
  BOOST_CHECK((unsigned)el1.inverseAdd() == 1);
  BOOST_CHECK((unsigned)el2.inverseAdd() == 7);
  BOOST_CHECK((unsigned)el3.inverseAdd() == 10);

  BOOST_CHECK_THROW(el.inverseMul(), std::invalid_argument);
  BOOST_CHECK((unsigned)el1.inverseMul() == 1);
  BOOST_CHECK((unsigned)el2.inverseMul() == 6);
  BOOST_CHECK((unsigned)el3.inverseMul() == 12);
}

BOOST_AUTO_TEST_CASE(shifts) {
  unsigned prim = 41;
  GF gf(5, prim);
  GFElem elem(gf, 1);
  BOOST_CHECK((unsigned)(elem << 1U) == 2);
  elem <<= 5U;
  BOOST_CHECK_MESSAGE((unsigned)elem == 9, "elem = " << (unsigned)elem);
  BOOST_CHECK_MESSAGE((unsigned)(elem >> 3U) == 1, "elem = " << (unsigned)(elem >> 3U));
  elem >>= 4U;
  BOOST_CHECK_MESSAGE((unsigned)elem == 0, "elem = " << (unsigned)elem);
}

BOOST_AUTO_TEST_CASE(comparison) {
  GF gf(5, 21);
  GF gf1(5, 21);
  GF gf2(4, 21);
  GF gf3(5, 13);
  GFElem el(gf, 13);
  GFElem el1(gf, 13); BOOST_CHECK(el == el1);
  GFElem el2(gf1, 13); BOOST_CHECK(el == el2);
  GFElem el3(gf, 14); BOOST_CHECK(el != el3);
  GFElem el4(gf2, 13); BOOST_CHECK(el != el4);
  GFElem el5(gf3, 13); BOOST_CHECK(el != el5);
}

BOOST_AUTO_TEST_CASE(subst_one_test) {
  GF gf(4,19);
  GFElem el1(gf, 5);
  GFElem el2(gf, 10);
  GFElem el3(gf, 11);
  GFElem el4(gf, 14);
  BOOST_CHECK(el1.subst_one() == GFElem(gf, 0));
  BOOST_CHECK(el2.subst_one() == GFElem(gf, 0));
  BOOST_CHECK(el3.subst_one() == GFElem(gf, 1));
  BOOST_CHECK(el4.subst_one() == GFElem(gf, 1));
}

BOOST_AUTO_TEST_CASE(getCoeffs_test) {
  GF gf(5, 41);
  GFElem el(gf, 21);
  BOOST_CHECK(el.getCoeff(0) == 1);
  BOOST_CHECK(el.getCoeff(1) == 0);
  BOOST_CHECK(el.getCoeff(2) == 1);
  BOOST_CHECK(el.getCoeff(3) == 0);
  BOOST_CHECK(el.getCoeff(4) == 1);
}

BOOST_AUTO_TEST_SUITE_END()
