#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BigNumber_test
#include <boost/test/unit_test.hpp>
#include <ippcp.h>
#include "include/bignum.h"

// Common functions test

BOOST_AUTO_TEST_SUITE(bn_number)

BOOST_AUTO_TEST_CASE(creation) {
  BigNumber zero;
}

BOOST_AUTO_TEST_CASE(creation1) {
  BigNumber unsBN(0x12345U);
}

BOOST_AUTO_TEST_CASE(creation2) {
  BigNumber signBN(-0x12345);
}

BOOST_AUTO_TEST_CASE(creation3) {
  BigNumber signBN(-0x12345);
  BigNumber copyBN(signBN);
  BOOST_CHECK(signBN == copyBN);
}

BOOST_AUTO_TEST_CASE(comparison){
  BigNumber less_zero("-0x12345");
  BigNumber greater_zero("0x12345");
  BigNumber greater_zero1("0x12345");
  BOOST_CHECK(less_zero < greater_zero);
  BOOST_CHECK(greater_zero > less_zero);
  BOOST_CHECK(greater_zero == greater_zero1);
  BOOST_CHECK(greater_zero != less_zero);
  BOOST_CHECK(greater_zero >= greater_zero);
  BOOST_CHECK(greater_zero <= greater_zero1);
}

BOOST_AUTO_TEST_CASE(constants) {
  BigNumber my_zero("0x0");
  BigNumber my_one("0x1");
  BigNumber my_two("0x2");
  BOOST_CHECK(my_zero = BigNumber::Zero());
  BOOST_CHECK(my_one = BigNumber::One());
  BOOST_CHECK(my_two = BigNumber::Two());
}

BOOST_AUTO_TEST_CASE(conversion_to_unsigned) {
  BOOST_CHECK_MESSAGE(BigNumber(0).uns() == 0, BigNumber(0).uns());
  BOOST_CHECK_MESSAGE(BigNumber(1).uns() == 1, BigNumber(1).uns());
  BOOST_CHECK_MESSAGE(BigNumber(-5).uns() == 5, BigNumber(-5).uns());
  BOOST_CHECK_MESSAGE(BigNumber(12345).uns() == 12345, BigNumber(12345).uns());
  BOOST_CHECK_MESSAGE(BigNumber(54321).uns() == 54321, BigNumber(54321).uns());
}

BOOST_AUTO_TEST_CASE(array_independency) {
  int new_bn_size = 5;
  // get the size of the Big Number context
  int ctxSize;
  ippsBigNumGetSize(new_bn_size, &ctxSize);
  // allocate the Big Number context
  IppsBigNumState* new_bn_state = (IppsBigNumState*) (new Ipp8u [ctxSize] ); 
  // and initialize one
  ippsBigNumInit(new_bn_size, new_bn_state);
  // if any data was supplied, then set up the Big Number value
  Ipp32u * pData = new Ipp32u[5];
  for(int i = 0; i < 5; ++i)
    pData[i] = i+2;
  // copy pData
  ippsSet_BN(IppsBigNumPOS, new_bn_size, pData, new_bn_state);
  pData[3] = 42;
  IppsBigNumSGN sgn;
  int bitlen;
  Ipp32u * new_pData;
  // new_pData - reference
  ippsRef_BN(&sgn, &bitlen, &new_pData, new_bn_state);
  BOOST_CHECK(new_pData[3] != pData[3]); // copy in ippsSet_BN
  new_pData[4] = 4242;
  Ipp32u * new_pData2;
  ippsRef_BN(&sgn, &bitlen, &new_pData2, new_bn_state);
  BOOST_CHECK(new_pData2[4] == new_pData[4]); // no copy in ippsRef_BN
}

BOOST_AUTO_TEST_CASE(num_independency) {
  BigNumber number1(1234567U);
  BigNumber number2(number1);
  BigNumber number3 = number1;
  BOOST_CHECK(number1 == number2);
  BOOST_CHECK(number1 == number3);
  number1 += 1;
  BOOST_CHECK(number1 != number2);
  BOOST_CHECK(number1 != number3);
}

BOOST_AUTO_TEST_SUITE_END()

// Math functions test.

BOOST_AUTO_TEST_SUITE(bn_math)

// + - * / % += -= *= /= %=
BOOST_AUTO_TEST_CASE(plus) {
  char * value1 = "0x11FF111";
  char * value2 = "0x2222222";
  char * result = "0x3421333";
  BigNumber res_num(result);
  BigNumber num1(value1);
  BigNumber num2(value2);
  BigNumber num3 = num1 + num2;
  BOOST_CHECK(res_num == num3);
  num2 += num1;
  BOOST_CHECK(res_num == num2);
}

BOOST_AUTO_TEST_CASE(munus) {
  char * value1 = "0x2500DEF";
  char * value2 = "0x2222222";
  char * result =  "0x2DEBCD";
  BigNumber res_num(result);
  BigNumber num1(value1);
  BigNumber num2(value2);
  BigNumber num3 = num1 - num2;
  BOOST_CHECK(res_num == num3);
  num1 -= num2;
  BOOST_CHECK(res_num == num1);
}

BOOST_AUTO_TEST_CASE(multiply) {
  char * value1 = "0x1111111";
  char * value2 = "0x2222222";
  char * result = "0x2468ACECA8642";
  BigNumber res_num(result);
  BigNumber num1(value1);
  BigNumber num2(value2);
  BigNumber num3 = num1 * num2;
  BOOST_CHECK(res_num == num3);
  num2 *= num1;
  BOOST_CHECK(res_num == num2);
}

BOOST_AUTO_TEST_CASE(divide) {
  char * value1 = "0xFEDCBA9";
  char * value2 = "0x16E5";
  char * result = "0xB21C";
  BigNumber res_num(result);
  BigNumber num1(value1);
  BigNumber num2(value2);
  BigNumber num3 = num1 / num2;
  BOOST_CHECK(res_num == num3);
  num1 /= num2;
  BOOST_CHECK(res_num == num1);
}

BOOST_AUTO_TEST_CASE(rem) {
  char * value = "0xFEDCBA9";
  BigNumber num(value);
  BOOST_CHECK((num%BigNumber(0x2U)) == BigNumber::One());
  BOOST_CHECK((num%BigNumber(0x3U)) == BigNumber::Zero());
  BOOST_CHECK((num%BigNumber(0x42U)) == BigNumber(0x1BU));
  num %= BigNumber(0x5U);
  BOOST_CHECK(num == BigNumber(0x4U));
  num %= BigNumber(0x3U);
  BOOST_CHECK(num == BigNumber::One());
}

BOOST_AUTO_TEST_CASE(pow) {
  BigNumber num1(0U);
  BigNumber num2(1U);
  BigNumber num3(2U);
  BigNumber num4(3U);
  BigNumber num5(4U);
  BigNumber num6(5U);
  BigNumber num7(-1);
  BigNumber num8(-3);

  {
    BigNumber bn1(num1);
    BOOST_CHECK_THROW(bn1.bpow(BigNumber::Zero()), std::invalid_argument);
    BigNumber bn2(num2); bn2.bpow(BigNumber::Zero());
    BigNumber bn3(num3); bn3.bpow(BigNumber::Zero());
    BigNumber bn4(num4); bn4.bpow(BigNumber::Zero());
    BigNumber bn5(num5); bn5.bpow(BigNumber::Zero());
    BigNumber bn6(num6); bn6.bpow(BigNumber::Zero());
    BigNumber bn7(num7); bn7.bpow(BigNumber::Zero());
    BigNumber bn8(num8); bn8.bpow(BigNumber::Zero());
    BOOST_CHECK(bn2 == BigNumber::One());
    BOOST_CHECK(bn3 == BigNumber::One());
    BOOST_CHECK(bn4 == BigNumber::One());
    BOOST_CHECK(bn5 == BigNumber::One());
    BOOST_CHECK(bn6 == BigNumber::One());
    BOOST_CHECK(bn7 == BigNumber::One());
    BOOST_CHECK(bn8 == BigNumber::One());
  }

  {
    BigNumber bn1(num1); bn1.bpow(BigNumber::One());
    BigNumber bn2(num2); bn2.bpow(BigNumber::One());
    BigNumber bn3(num3); bn3.bpow(BigNumber::One());
    BigNumber bn4(num4); bn4.bpow(BigNumber::One());
    BigNumber bn5(num5); bn5.bpow(BigNumber::One());
    BigNumber bn6(num6); bn6.bpow(BigNumber::One());
    BigNumber bn7(num7); bn7.bpow(BigNumber::One());
    BigNumber bn8(num8); bn8.bpow(BigNumber::One());
    BOOST_CHECK(bn1 == num1);
    BOOST_CHECK(bn2 == num2);
    BOOST_CHECK(bn3 == num3);
    BOOST_CHECK(bn4 == num4);
    BOOST_CHECK(bn5 == num5);
    BOOST_CHECK(bn6 == num6);
    BOOST_CHECK(bn7 == num7);
    BOOST_CHECK(bn8 == num8);
  }

  {
    BigNumber bn1(num1); bn1.bpow(BigNumber::Two());
    BigNumber bn2(num2); bn2.bpow(BigNumber::Two());
    BigNumber bn3(num3); bn3.bpow(BigNumber::Two());
    BigNumber bn4(num4); bn4.bpow(BigNumber::Two());
    BigNumber bn5(num5); bn5.bpow(BigNumber::Two());
    BigNumber bn6(num6); bn6.bpow(BigNumber::Two());
    BigNumber bn7(num7); bn7.bpow(BigNumber::Two());
    BigNumber bn8(num8); bn8.bpow(BigNumber::Two());
    BOOST_CHECK(bn1 == BigNumber::Zero());
    BOOST_CHECK(bn2 == BigNumber::One());
    BOOST_CHECK(bn3 == BigNumber("4"));
    BOOST_CHECK(bn4 == BigNumber("9"));
    BOOST_CHECK(bn5 == BigNumber("16"));
    BOOST_CHECK(bn6 == BigNumber("25"));
    BOOST_CHECK(bn7 == BigNumber::One());
    BOOST_CHECK(bn8 == BigNumber("9"));
  }

  {
    BigNumber bn1(num1); bn1.bpow(BigNumber("5"));
    BigNumber bn2(num2); bn2.bpow(BigNumber("5"));
    BigNumber bn3(num3); bn3.bpow(BigNumber("5"));
    BigNumber bn4(num4); bn4.bpow(BigNumber("5"));
    BigNumber bn5(num5); bn5.bpow(BigNumber("5"));
    BigNumber bn6(num6); bn6.bpow(BigNumber("5"));
    BigNumber bn7(num7); bn7.bpow(BigNumber("5"));
    BigNumber bn8(num8); bn8.bpow(BigNumber("5"));
    BOOST_CHECK(bn1 == BigNumber::Zero());
    BOOST_CHECK(bn2 == BigNumber::One());
    BOOST_CHECK(bn3 == BigNumber("32"));
    BOOST_CHECK(bn4 == BigNumber("243"));
    BOOST_CHECK(bn5 == BigNumber("1024"));
    BOOST_CHECK(bn6 == BigNumber("3125"));
    BOOST_CHECK(bn7 == BigNumber(-1));
    BOOST_CHECK(bn8 == BigNumber("-243"));
  }
}

BOOST_AUTO_TEST_CASE(babs) {
  BigNumber num1(0); num1.babs();
  BigNumber num2(5); num2.babs();
  BigNumber num3(-5); num3.babs();
  BOOST_CHECK(num1 == BigNumber::Zero());
  BOOST_CHECK(num2 == BigNumber(5));
  BOOST_CHECK(num3 == BigNumber(5));
}

BOOST_AUTO_TEST_CASE(bsqrt) {
  BigNumber num1(-1); 
  BOOST_CHECK_THROW(num1.bsqrt(), std::invalid_argument);

  BigNumber num2(0); num2.bsqrt();
  BigNumber num3(1); num3.bsqrt();
  BigNumber num4(9); num4.bsqrt();
  BigNumber num5("1234567890987654321"); num5.bsqrt();
  
  BOOST_CHECK(num2 == BigNumber::Zero());
  BOOST_CHECK(num3 == BigNumber::One());
  BOOST_CHECK(num4 == BigNumber(3));
  BOOST_CHECK(num5 == BigNumber("1111111106"));
}

BOOST_AUTO_TEST_CASE(ln_error) {
  BigNumber num("-10");
  BOOST_CHECK_THROW(loge(num), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(ln) {
  char * value = "123456789987654321"; //dec
  BigNumber num(value);
  BOOST_CHECK(abs(loge(num) - 39.354667611) < 0.00001);
}

BOOST_AUTO_TEST_CASE(ln_small) {
  char * value = "12345";
  BigNumber num(value);
  BOOST_CHECK(abs(loge(num) - log(12345)) < 0.00001);
}

BOOST_AUTO_TEST_CASE(ln_large) {
  const int N = 100;
  char value[N+2];
  for (int i = 2; i < N+2; ++i) {
    value[i] = '9';
  }
  value[0] = '0'; value[1] = 'x';
  BigNumber num(value);
  BOOST_CHECK(abs(loge(num) - 276.7480466) < 0.00001);
}

BOOST_AUTO_TEST_SUITE_END()

// Modulo arthmetic test.

BOOST_AUTO_TEST_SUITE(modulo_arithmetic)

BOOST_AUTO_TEST_CASE(modulo_error) {
  BigNumber mod1("0x0");
  BigNumber mod2("0x1");
  BigNumber mod3("-0x10");
  BOOST_CHECK_THROW(mod1.Modulo(BigNumber("5")), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.Modulo(BigNumber("5")), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.Modulo(BigNumber("5")), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(modulo){
  BigNumber num1("0x0");
  BigNumber num2("0x5");
  BigNumber num3("0x42");
  BigNumber num4("0x123456789ABCEF");
  BigNumber num5("-0x1");

  BigNumber modulo3("0x5");
  BigNumber modulo4("0x121");
  BigNumber modulo5("0x123456789");

  BOOST_CHECK(modulo3.Modulo(num1) == BigNumber("0x0"));
  BOOST_CHECK(modulo4.Modulo(num1) == BigNumber("0x0"));
  BOOST_CHECK(modulo5.Modulo(num1) == BigNumber("0x0"));

  BOOST_CHECK(modulo3.Modulo(num2) == BigNumber("0x0"));
  BOOST_CHECK(modulo4.Modulo(num2) == BigNumber("0x5"));
  BOOST_CHECK(modulo5.Modulo(num2) == BigNumber("0x5"));

  BOOST_CHECK(modulo3.Modulo(num3) == BigNumber("0x1"));
  BOOST_CHECK(modulo4.Modulo(num3) == BigNumber("0x42"));
  BOOST_CHECK(modulo5.Modulo(num3) == BigNumber("0x42"));

  BOOST_CHECK(modulo3.Modulo(num4) == BigNumber("0x2"));
  BOOST_CHECK(modulo4.Modulo(num4) == BigNumber("0x8F"));
  BOOST_CHECK(modulo5.Modulo(num4) == BigNumber("0xABCEF"));

  BOOST_CHECK(modulo3.Modulo(num5) == BigNumber("0x4"));
  BOOST_CHECK(modulo4.Modulo(num5) == BigNumber("0x120"));
  BOOST_CHECK(modulo5.Modulo(num5) == BigNumber("0x123456788"));

  BOOST_CHECK(BigNumber::Two().Modulo(BigNumber(24961)) == BigNumber::One());
}

BOOST_AUTO_TEST_CASE(modulo_ops_error) {
  BigNumber mod1("0x0");
  BigNumber mod2("0x1");
  BigNumber mod3("-0x10");
  BigNumber a("5");
  BigNumber b("42");
  BOOST_CHECK_THROW(mod1.ModAdd(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod1.ModSub(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod1.ModMul(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod1.ModPow(a, b), std::invalid_argument);

  BOOST_CHECK_THROW(mod2.ModAdd(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.ModSub(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.ModMul(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.ModPow(a, b), std::invalid_argument);

  BOOST_CHECK_THROW(mod3.ModAdd(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod3.ModSub(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod3.ModMul(a, b), std::invalid_argument);
  BOOST_CHECK_THROW(mod3.ModPow(a, b), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(modulo_ops) {
  BigNumber modulo("42");
  BOOST_CHECK(modulo.ModAdd(BigNumber("5"), BigNumber("5")) == BigNumber("10"));
  BOOST_CHECK(modulo.ModSub(BigNumber("10"), BigNumber("5")) == BigNumber("5"));
  BOOST_CHECK(modulo.ModMul(BigNumber("5"), BigNumber("5")) == BigNumber("25"));
  BOOST_CHECK(modulo.ModPow(BigNumber("5"), BigNumber("2")) == BigNumber("25"));

  BOOST_CHECK(modulo.ModAdd(BigNumber("40"), BigNumber("5")) == BigNumber("3"));
  BOOST_CHECK(modulo.ModSub(BigNumber("5"), BigNumber("10")) == BigNumber("37"));
  BOOST_CHECK(modulo.ModMul(BigNumber("7"), BigNumber("7")) == BigNumber("7"));
  BOOST_CHECK(modulo.ModPow(BigNumber("7"), BigNumber("2")) == BigNumber("7"));

  BOOST_CHECK_THROW(modulo.ModPow(BigNumber::Zero(), BigNumber::Zero()), std::invalid_argument);

  BOOST_CHECK(BigNumber(2).ModPow(BigNumber(24961), BigNumber(1)) == BigNumber(1));
}

BOOST_AUTO_TEST_CASE(modulo_inverse_error) {
  BigNumber mod1("0x0");
  BigNumber mod2("0x1");
  BigNumber mod3("-0x10");
  BigNumber a("5");

  BOOST_CHECK_THROW(mod1.InverseAdd(a), std::invalid_argument);
  BOOST_CHECK_THROW(mod1.InverseMul(a), std::invalid_argument);

  BOOST_CHECK_THROW(mod2.InverseAdd(a), std::invalid_argument);
  BOOST_CHECK_THROW(mod2.InverseMul(a), std::invalid_argument);

  BOOST_CHECK_THROW(mod3.InverseAdd(a), std::invalid_argument);
  BOOST_CHECK_THROW(mod3.InverseMul(a), std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(modulo_inverse){
  BigNumber modulo(42);

  BOOST_CHECK(modulo.InverseAdd(BigNumber::Zero()) == BigNumber::Zero());
  BOOST_CHECK_THROW(modulo.InverseMul(BigNumber::Zero()), std::invalid_argument);

  BOOST_CHECK(modulo.InverseAdd(BigNumber("5")) == BigNumber("37"));
  BOOST_CHECK(modulo.InverseAdd(BigNumber("-37")) == BigNumber("37"));
  BOOST_CHECK(modulo.InverseMul(BigNumber("5")) == BigNumber("17"));
  BOOST_CHECK(modulo.InverseMul(BigNumber("-17")) == BigNumber("37"));

  BigNumber modulo1(2);
  // std::cout << modulo1.InverseMul(BigNumber(24961)) << std::endl;

  BOOST_CHECK(modulo1.InverseMul(BigNumber(24961)) == BigNumber::One());
  BOOST_CHECK(modulo1.InverseMul(BigNumber(1)) == BigNumber(1));
}

BOOST_AUTO_TEST_CASE(legendre_symbol){
  BigNumber modulo("29");

  BOOST_CHECK(modulo.legendre_symbol(BigNumber("29")) == BigNumber::Zero());
  BOOST_CHECK(modulo.legendre_symbol(BigNumber("1")) == BigNumber::One());
  BOOST_CHECK(modulo.legendre_symbol(BigNumber("-1")) == BigNumber::One());
  BOOST_CHECK(modulo.legendre_symbol(BigNumber("4")) == BigNumber::One());
  BOOST_CHECK(modulo.legendre_symbol(BigNumber("8")) == modulo.Modulo(BigNumber(-1)));
}

BOOST_AUTO_TEST_SUITE_END()
