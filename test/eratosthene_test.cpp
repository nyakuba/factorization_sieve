#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Eratosthene_test
#include <boost/test/unit_test.hpp>
#include <fstream>
#include "include/eratosthenes.h"

BOOST_AUTO_TEST_SUITE(er_sieve_test)

BOOST_AUTO_TEST_CASE(creation){
  Eratosthene sieve();
}

BOOST_AUTO_TEST_CASE(sieve_little){
  const unsigned N = 50;
  std::fstream in("test/tests/eratosthene_little.out");
  BOOST_REQUIRE(in.is_open());
  std::vector<unsigned> answer;
  unsigned tmp = 0;
  while(!in.eof()){
    in >> tmp;
    answer.push_back(tmp);
  }
  in.close();
  BOOST_REQUIRE(!in.is_open());

  Eratosthene sieve;

  // for(auto && el : sieve.m_primes)
  //   std::cout << el << std::endl;


  sieve.gen_primes(N);
  BOOST_REQUIRE_MESSAGE(answer.size() == sieve.m_primes.size(), 
			"fail: answer.size() == " << answer.size() << ", primes.size() = " << sieve.m_primes.size());

  unsigned index = 0;
  while(index < answer.size()){
    BOOST_REQUIRE(answer[index] == sieve.m_primes[index]);
    ++index;
  }
}

BOOST_AUTO_TEST_CASE(sieve_small){
  const unsigned N = 5000;
  std::fstream in("test/tests/eratosthene_small.out");
  BOOST_REQUIRE(in.is_open());
  std::vector<unsigned> answer;
  unsigned tmp = 0;
  while(!in.eof()){
    in >> tmp;
    answer.push_back(tmp);
  }
  answer.erase(answer.end()-1);
  in.close();
  BOOST_REQUIRE(!in.is_open());

  Eratosthene sieve;
  sieve.gen_primes(N);
  // for (auto && el : sieve.m_primes)
  //   std::cout << el << " ";
  // std::cout << std::endl;


  BOOST_CHECK(answer.size() == sieve.m_primes.size());

  unsigned index = 0;
  while(index < answer.size()){
    BOOST_REQUIRE_MESSAGE(answer[index] == sieve.m_primes[index], 
			  "fail on index = " << index << 
			  ", answer = " << answer[index] <<
			  ", primes = " << sieve.m_primes[index]);
    ++index;
  }
}

BOOST_AUTO_TEST_CASE(sieve_large){
  const unsigned N = 500000;
  std::fstream in("test/tests/eratosthene_large.out");
  BOOST_REQUIRE(in.is_open());
  std::vector<unsigned> answer;
  unsigned tmp = 0;
  while(!in.eof()){
    in >> tmp;
    answer.push_back(tmp);
  }
  answer.erase(answer.end()-1);
  in.close();
  BOOST_REQUIRE(!in.is_open());

  Eratosthene sieve;
  sieve.gen_primes(N);
  BOOST_CHECK(answer.size() == sieve.m_primes.size());

  unsigned index = 0;
  while(index < answer.size()){
    BOOST_REQUIRE_MESSAGE(answer[index] == sieve.m_primes[index], 
			  "fail on index = " << index << 
			  ", answer = " << answer[index] <<
			  ", primes = " << sieve.m_primes[index]);
    ++index;
  }
}

BOOST_AUTO_TEST_CASE(sieve_little_then_small){
  // ������� ������� ����� ����������, ����� ��������.
  const unsigned N = 50;
  std::vector<unsigned> answer;
  unsigned tmp = 0;
  Eratosthene sieve;
  unsigned index = 0;
  {
    std::fstream in("test/tests/eratosthene_little.out");
    BOOST_REQUIRE(in.is_open());
    while(!in.eof()){
      in >> tmp;
      answer.push_back(tmp);
    }
    in.close();
    BOOST_REQUIRE(!in.is_open());
  }

  sieve.gen_primes(N);
  BOOST_REQUIRE(answer.size() == sieve.m_primes.size());

  while(index < answer.size()){
    BOOST_REQUIRE(answer[index] == sieve.m_primes[index]);
    ++index;
  }

  // ���� ������ ������ �� ���������.
  // �������� ����������.
  const unsigned M = 5000;
  {
    std::fstream in("test/tests/eratosthene_small.out");
    BOOST_REQUIRE(in.is_open());
    answer.clear();
    while(!in.eof()){
      in >> tmp;
      answer.push_back(tmp);
    }
    answer.erase(answer.end()-1);
    in.close();
    BOOST_REQUIRE(!in.is_open());
  }

  sieve.gen_primes(M);
  BOOST_CHECK(answer.size() == sieve.m_primes.size());

  while(index < answer.size()){
    BOOST_REQUIRE_MESSAGE(answer[index] == sieve.m_primes[index], 
			  "fail on index = " << index << 
			  ", answer = " << answer[index] <<
			  ", primes = " << sieve.m_primes[index]);
    ++index;
  }
}

BOOST_AUTO_TEST_SUITE_END()
