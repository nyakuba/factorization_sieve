#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE main_test
#include <boost/test/unit_test.hpp>

#include "../sample.cpp"

BOOST_AUTO_TEST_CASE(test_sub) {
  BOOST_CHECK(sub(2,2) == 0);
}
