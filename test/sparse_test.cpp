#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Sparse_test
#include <boost/test/unit_test.hpp>
#include "include/gf.h"
#include "include/gfelem.h"
#include "include/sparse.h"

// common operations
BOOST_AUTO_TEST_SUITE(sparse_common)

BOOST_AUTO_TEST_CASE(creation) {
  GF gf(4,19);
  GFElem one(gf, 1);
  SMatrix m1(5, 6, one);

  BOOST_CHECK(m1.getN() == 5);
  BOOST_CHECK(m1.getM() == 6);
  
  unsigned N = 10;
  unsigned M = 41;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 1));  v.push_back(GFElem(gf, 2));  v.push_back(GFElem(gf, 3));
  v.push_back(GFElem(gf, 4));  v.push_back(GFElem(gf, 5));  v.push_back(GFElem(gf, 6));
  v.push_back(GFElem(gf, 7));  v.push_back(GFElem(gf, 8));  v.push_back(GFElem(gf, 9));
  std::vector<unsigned> r = {0, 0, 3, 3, 3, 4, 6, 7, 8, 9, 9};
  std::vector<unsigned> c = {0, 23, 24, 31, 5, 2, 12, 37, 0};  
  SMatrix m2(N,M,one,v,r,c);
  SMatrix m3(m2);

  BOOST_CHECK(m2.getN() == N);
  BOOST_CHECK(m2.getM() == M);
  BOOST_CHECK(m3.getN() == N);
  BOOST_CHECK(m3.getM() == M);
}

BOOST_AUTO_TEST_CASE(set_get) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7));
  v.push_back(GFElem(gf, 8)); v.push_back(GFElem(gf, 9));
  /* 5 0 0 0 0 0 0
     0 0 6 0 0 0 0
     0 0 0 0 0 0 0
     7 0 0 0 0 0 8
     0 0 0 0 0 0 0
     0 0 0 0 0 0 9 */
  std::vector<unsigned> r = {0, 1, 2, 2, 4, 4, 5};
  std::vector<unsigned> c = {0, 2, 0, 6, 6};  
  SMatrix matrix(N,M,zero,v,r,c);
  
  BOOST_CHECK(matrix.get(0,0) == GFElem(gf, 5));
  BOOST_CHECK(matrix.get(1,2) == GFElem(gf, 6));
  BOOST_CHECK(matrix.get(3,0) == GFElem(gf, 7));
  BOOST_CHECK(matrix.get(3,6) == GFElem(gf, 8));
  BOOST_CHECK(matrix.get(5,6) == GFElem(gf, 9));

  v[2] = GFElem(gf, 13);
  BOOST_CHECK(matrix.get(3,0) == GFElem(gf, 7));

  matrix.set(1,2, GFElem(gf, 11));
  matrix.set(3,0, GFElem(gf, 12));
  matrix.set(3,6, GFElem(gf, 13));
  matrix.set(5,6, GFElem(gf, 14));
  BOOST_CHECK(matrix.get(1,2) == GFElem(gf, 11));
  BOOST_CHECK(matrix.get(3,0) == GFElem(gf, 12));
  BOOST_CHECK(matrix.get(3,6) == GFElem(gf, 13));
  BOOST_CHECK(matrix.get(5,6) == GFElem(gf, 14));

  matrix.set(0,1, GFElem(gf, 1));
  matrix.set(0,2, GFElem(gf, 2));
  matrix.set(0,6, GFElem(gf, 3));
  BOOST_CHECK(matrix.get(0,1) == GFElem(gf, 1));
  BOOST_CHECK(matrix.get(0,2) == GFElem(gf, 2));
  BOOST_CHECK(matrix.get(0,6) == GFElem(gf, 3));

  matrix.set(2,4, GFElem(gf, 2));
  matrix.set(3,4, GFElem(gf, 3));
  matrix.set(5,4, GFElem(gf, 4));
  BOOST_CHECK(matrix.get(2,4) == GFElem(gf, 2));
  BOOST_CHECK(matrix.get(3,4) == GFElem(gf, 3));
  BOOST_CHECK(matrix.get(5,4) == GFElem(gf, 4));
}

BOOST_AUTO_TEST_CASE(set_zero) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7));
  v.push_back(GFElem(gf, 8)); v.push_back(GFElem(gf, 9));
  /* 5 0 0 0 0 0 0
     0 0 6 0 0 0 0
     0 0 0 0 0 0 0
     7 0 0 0 0 0 8
     0 0 0 0 0 0 0
     0 0 0 0 0 0 9 */
  std::vector<unsigned> r = {0, 1, 2, 2, 4, 4, 5};
  std::vector<unsigned> c = {0, 2, 0, 6, 6};  
  SMatrix matrix(N,M,zero,v,r,c);
  
  matrix.set(0,0, zero);
  BOOST_CHECK(matrix.get(0,0) == zero);
  BOOST_CHECK(matrix.get(1,2) == GFElem(gf, 6));
  matrix.set(1,2, zero);
  BOOST_CHECK(matrix.get(1,2) == zero);
  BOOST_CHECK(matrix.get(3,0) == GFElem(gf, 7));
  matrix.set(5,6, zero);
  BOOST_CHECK(matrix.get(5,6) == zero);
  BOOST_CHECK(matrix.get(3,6) == GFElem(gf, 8));
}

BOOST_AUTO_TEST_CASE(copy) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7));
  v.push_back(GFElem(gf, 8)); v.push_back(GFElem(gf, 9));
  std::vector<unsigned> r = {0, 1, 2, 2, 4, 4, 5};
  std::vector<unsigned> c = {0, 2, 0, 6, 6};  
  SMatrix matrix(N,M,zero,v,r,c);
  
  SMatrix matrix1(matrix);
  BOOST_CHECK(matrix1.get(0,0) == GFElem(gf, 5));
  BOOST_CHECK(matrix1.get(1,2) == GFElem(gf, 6));
  BOOST_CHECK(matrix1.get(3,0) == GFElem(gf, 7));
  BOOST_CHECK(matrix1.get(3,6) == GFElem(gf, 8));
  BOOST_CHECK(matrix1.get(5,6) == GFElem(gf, 9));

  matrix.set(0, 0, GFElem(gf, 10));
  BOOST_CHECK(matrix.get(0,0) == GFElem(gf, 10));
  BOOST_CHECK(matrix1.get(0,0) == GFElem(gf, 5));
}

BOOST_AUTO_TEST_CASE(get_row_col) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v = {GFElem(gf, 5), GFElem(gf, 1), GFElem(gf, 6), GFElem(gf, 6), GFElem(gf, 7), 
			   GFElem(gf, 4), GFElem(gf, 2), GFElem(gf, 8), GFElem(gf, 3), GFElem(gf, 9)};
  /* 5 0 0 1 0 0 0
     0 0 6 0 0 6 0
     0 0 0 0 0 0 0
     7 4 0 2 0 0 8
     0 0 0 0 0 0 0
     0 0 0 0 3 0 9 */
  std::vector<unsigned> r = {0, 2, 4, 4, 8, 8, 10};
  std::vector<unsigned> c = {0, 3, 2, 5, 0, 1, 3, 6, 4, 6};  
  SMatrix matrix(N,M,zero,v,r,c);

  std::vector<GFElem> first_row = {GFElem(gf, 5), zero, zero, GFElem(gf, 1), zero, zero, zero};
  std::vector<GFElem> row3 = {GFElem(gf, 7), GFElem(gf, 4), zero, GFElem(gf, 2), zero, zero, GFElem(gf, 8)};
  std::vector<GFElem> last_row = {zero, zero, zero, zero, GFElem(gf, 3), zero, GFElem(gf, 9)};

  std::vector<GFElem> first_column = {GFElem(gf, 5), zero, zero, GFElem(gf, 7), zero, zero};
  std::vector<GFElem> column3 = {GFElem(gf, 1), zero, zero, GFElem(gf, 2), zero, zero};
  std::vector<GFElem> last_column = {zero, zero, zero, GFElem(gf, 8), zero, GFElem(gf, 9)};

  std::vector<GFElem> ret;
  ret = matrix.getRow(0);
  BOOST_REQUIRE(first_row.size() == ret.size());
  for (unsigned i = 0; i < first_row.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == first_row[i], "fail on i = " << i 
			<< ": first_row " << first_row[i] << ", ret " << ret[i]);

  ret = matrix.getRow(3);
  BOOST_REQUIRE(row3.size() == ret.size());
  for (unsigned i = 0; i < row3.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == row3[i], "fail on i = " << i 
			<< ": row3 " << row3[i] << ", ret " << ret[i]);
  
  ret = matrix.getRow(5);
  BOOST_REQUIRE(last_row.size() == ret.size());
  for (unsigned i = 0; i < last_row.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == last_row[i], "fail on i = " << i 
			<< ": last_row " << last_row[i] << ", ret " << ret[i]);

  ret = matrix.getColumn(0);
  BOOST_REQUIRE(first_column.size() == ret.size());
  for (unsigned i = 0; i < first_column.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == first_column[i], "fail on i = " << i 
			<< ": first_column " << first_column[i] << ", ret " << ret[i]);

  ret = matrix.getColumn(3);
  BOOST_REQUIRE(column3.size() == ret.size());
  for (unsigned i = 0; i < column3.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == column3[i], "fail on i = " << i 
			<< ": column3 " << column3[i] << ", ret " << ret[i]);

  ret = matrix.getColumn(6);
  BOOST_REQUIRE(last_column.size() == ret.size());
  for (unsigned i = 0; i < last_column.size(); ++i)
    BOOST_CHECK_MESSAGE(ret[i] == last_column[i], "fail on i = " << i 
			<< ": last_column " << last_column[i] << ", ret " << ret[i]);
}

BOOST_AUTO_TEST_CASE(transpose_test) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v = {GFElem(gf, 5), GFElem(gf, 1), GFElem(gf, 6), GFElem(gf, 6), GFElem(gf, 7), 
			   GFElem(gf, 4), GFElem(gf, 2), GFElem(gf, 8), GFElem(gf, 3), GFElem(gf, 9)};
  /* 5 0 0 1 0 0 0
     0 0 6 0 0 6 0
     0 0 0 0 0 0 0
     7 4 0 2 0 0 8
     0 0 0 0 0 0 0
     0 0 0 0 3 0 9 */
  std::vector<unsigned> r = {0, 2, 4, 4, 8, 8, 10};
  std::vector<unsigned> c = {0, 3, 2, 5, 0, 1, 3, 6, 4, 6};  
  SMatrix matrix(N,M,zero,v,r,c);

  SMatrix matrixt(matrix.transpose());

  std::vector<GFElem> expv = {GFElem(gf, 5), GFElem(gf, 7), GFElem(gf, 4), GFElem(gf, 6), GFElem(gf, 1), 
			      GFElem(gf, 2), GFElem(gf, 3), GFElem(gf, 6), GFElem(gf, 8), GFElem(gf, 9)};
  /* 5 0 0 7 0 0
     0 0 0 4 0 0
     0 6 0 0 0 0
     1 0 0 2 0 0
     0 0 0 0 0 3
     0 6 0 0 0 0 
     0 0 0 8 0 9 */
  std::vector<unsigned> expr = {0, 2, 3, 4, 6, 7, 8, 10};
  std::vector<unsigned> expc = {0, 3, 3, 1, 0, 3, 5, 1, 3, 5};
  SMatrix expected(M, N, zero, expv, expr, expc);
  // matrixt.printData();
  // expected.printData();

  BOOST_REQUIRE(expected.getN() == matrixt.getN());
  BOOST_REQUIRE(expected.getM() == matrixt.getM());
  for (unsigned row = 0; row < matrixt.getN(); ++row)
    for (unsigned column = 0; column < matrixt.getM(); ++column)
      BOOST_CHECK_MESSAGE(expected.get(row, column) == matrixt.get(row, column), 
			  "fail on (" << row << "," << column << "): expected " <<
			  expected.get(row, column) << ", matrixt " << matrixt.get(row, column));  
  
}

BOOST_AUTO_TEST_SUITE_END()

// matrix and vector arithmetic
BOOST_AUTO_TEST_SUITE(sparse_ops)

BOOST_AUTO_TEST_CASE(add) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6;
  unsigned M = 7;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7));
  v.push_back(GFElem(gf, 8)); v.push_back(GFElem(gf, 9));
  /* 5 0 0 0 0 0 0
     0 0 6 0 0 0 0
     0 0 0 0 0 0 0
     7 0 0 0 0 0 8
     0 0 0 0 0 0 0
     0 0 0 0 0 0 9 */
  std::vector<unsigned> r = {0, 1, 2, 2, 4, 4, 5};
  std::vector<unsigned> c = {0, 2, 0, 6, 6};  
  SMatrix lhs(N,M,zero,v,r,c);

  std::vector<GFElem> v1;  
  v1.push_back(GFElem(gf, 5)); v1.push_back(GFElem(gf, 2)); v1.push_back(GFElem(gf, 3));
  v1.push_back(GFElem(gf, 4)); v1.push_back(GFElem(gf, 5)); v1.push_back(GFElem(gf, 9));
  /* 5 0 0 0 0 0 0
     0 0 2 0 0 0 0
     0 0 0 0 3 0 4
     0 0 0 0 0 0 0
     5 0 0 0 0 0 0
     0 0 0 0 0 0 9 */
  std::vector<unsigned> r1 = {0, 1, 2, 4, 4, 5, 6};
  std::vector<unsigned> c1 = {0, 2, 4, 6, 0, 6};  
  SMatrix rhs(N,M,zero,v1,r1,c1);

  SMatrix result = lhs + rhs;
  /* Expected:
     0 0 0 0 0 0 0
     0 0 4 0 0 0 0
     0 0 0 0 3 0 4
     7 0 0 0 0 0 8
     5 0 0 0 0 0 0
     0 0 0 0 0 0 0 */
  unsigned expN = 6; unsigned expM = 7;
  std::vector<GFElem> expv;  
  expv.push_back(GFElem(gf, 4)); expv.push_back(GFElem(gf, 3)); 
  expv.push_back(GFElem(gf, 4)); expv.push_back(GFElem(gf, 7)); 
  expv.push_back(GFElem(gf, 8)); expv.push_back(GFElem(gf, 5));
  std::vector<unsigned> expr = {0, 0, 1, 3, 5, 6, 6};
  std::vector<unsigned> expc = {2, 4, 6, 0, 6, 0};  
  SMatrix expected(expN, expM, zero, expv, expr, expc);
  
  BOOST_REQUIRE(result.getN() == expN);
  BOOST_REQUIRE(result.getM() == expM);
  for (unsigned row = 0; row < result.getN(); ++row)
    for (unsigned column = 0; column < result.getM(); ++column)
      BOOST_CHECK_MESSAGE(expected.get(row, column) == result.get(row, column), 
			  "fail on (" << row << "," << column << "): expected " <<
			  expected.get(row, column) << ", result " << result.get(row, column));
}

BOOST_AUTO_TEST_CASE(mul) {
  GF gf(4,19);
  GFElem zero(gf, 0);
  unsigned N = 6; unsigned M = 7;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 3)); v.push_back(GFElem(gf, 2));
  v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7)); v.push_back(GFElem(gf, 8)); 
  v.push_back(GFElem(gf, 3)); v.push_back(GFElem(gf, 9));
  /* 5 0 0 3 0 2 0
     0 0 0 6 0 0 0
     0 0 0 0 0 0 0
     7 0 0 0 0 0 8
     0 0 0 0 0 0 0
     0 0 3 0 0 0 9 */
  std::vector<unsigned> r = {0, 3, 4, 4, 6, 6, 8};
  std::vector<unsigned> c = {0, 3, 5, 3, 0, 6, 2, 6};  
  SMatrix lhs(N,M,zero,v,r,c);

  std::vector<GFElem> v1;  
  v1.push_back(GFElem(gf, 5)); v1.push_back(GFElem(gf, 2)); v1.push_back(GFElem(gf, 3));
  v1.push_back(GFElem(gf, 2)); v1.push_back(GFElem(gf, 9));
  /* 0 5 0 0
     0 0 2 0 
     0 0 0 0
     0 3 0 0
     0 0 0 0
     0 2 0 0 
     0 0 0 9 */
  unsigned N1 = 7; unsigned M1 = 4;
  std::vector<unsigned> r1 = {0, 1, 2, 2, 3, 3, 4, 5};
  std::vector<unsigned> c1 = {1, 2, 1, 1, 3};  
  SMatrix rhs(N1,M1,zero,v1,r1,c1);
  
  SMatrix result = lhs * rhs;
  // result.printData();
  /* Expected:
     0  3  0  0
     0  10 0  0
     0  0  0  0
     0  8  0  4
     0  0  0  0
     0  0  0 13 */
  unsigned expN = 6; unsigned expM = 4;
  std::vector<GFElem> expv;  
  expv.push_back(GFElem(gf, 3)); expv.push_back(GFElem(gf, 10)); 
  expv.push_back(GFElem(gf, 8)); expv.push_back(GFElem(gf, 4)); expv.push_back(GFElem(gf, 13));
  std::vector<unsigned> expr = {0, 1, 2, 2, 4, 4, 5};
  std::vector<unsigned> expc = {1, 1, 1, 3, 3};  
  SMatrix expected(expN, expM, zero, expv, expr, expc);
  
  BOOST_REQUIRE(result.getN() == expN);
  BOOST_REQUIRE(result.getM() == expM);
  for (unsigned row = 0; row < result.getN(); ++row)
    for (unsigned column = 0; column < result.getM(); ++column)
      BOOST_CHECK_MESSAGE(expected.get(row, column) == result.get(row, column), 
			  "fail on (" << row << "," << column << "): expected " <<
			  expected.get(row, column) << ", result " << result.get(row, column));
}

BOOST_AUTO_TEST_CASE(vmul) {
  GF gf(4,19);
  GFElem zero(gf, 0);

  std::vector<GFElem> lhs;
  lhs.push_back(GFElem(gf, 1)); lhs.push_back(GFElem(gf, 2)); lhs.push_back(zero);
  lhs.push_back(GFElem(gf, 3)); lhs.push_back(GFElem(gf, 4)); lhs.push_back(GFElem(gf, 5));
  lhs.push_back(GFElem(gf, 6));
  /* 1 2 0 3 4 5 6 */

  std::vector<GFElem> v1;  
  v1.push_back(GFElem(gf, 5)); v1.push_back(GFElem(gf, 2)); v1.push_back(GFElem(gf, 3));
  v1.push_back(GFElem(gf, 2)); v1.push_back(GFElem(gf, 9));
  /* 0 5 0 0
     0 0 2 0 
     0 0 0 0
     0 3 0 0
     0 0 0 0
     0 2 0 0 
     0 0 0 9 */
  unsigned N1 = 7; unsigned M1 = 4;
  std::vector<unsigned> r1 = {0, 1, 2, 2, 3, 3, 4, 5};
  std::vector<unsigned> c1 = {1, 2, 1, 1, 3};  
  SMatrix rhs(N1,M1,zero,v1,r1,c1);

  std::vector<GFElem> expected;
  expected.push_back(zero); expected.push_back(GFElem(gf, 10)); 
  expected.push_back(GFElem(gf, 4)); expected.push_back(GFElem(gf, 3)); 
  /* 0, 10, 4, 3 */

  std::vector<GFElem> result = lhs*rhs;
  BOOST_REQUIRE(expected.size() == result.size());
  for (unsigned i = 0; i < expected.size(); ++i)
    BOOST_CHECK_MESSAGE(result[i] == expected[i], "fail on i = " << i 
			<< ": expected " << expected[i] << ", result " << result[i]);
}

BOOST_AUTO_TEST_CASE(mulv) {
  GF gf(4,19);
  GFElem zero(gf, 0);

  unsigned N = 5; unsigned M = 6;
  std::vector<GFElem> v;
  v.push_back(GFElem(gf, 5)); v.push_back(GFElem(gf, 3)); v.push_back(GFElem(gf, 2));
  v.push_back(GFElem(gf, 6)); v.push_back(GFElem(gf, 7)); v.push_back(GFElem(gf, 3));
  /* 5 0 0 3 0 2
     0 0 0 6 0 0
     0 0 0 0 0 0
     7 0 0 0 0 0
     0 0 3 0 0 0 */
  std::vector<unsigned> r = {0, 3, 4, 4, 5, 6};
  std::vector<unsigned> c = {0, 3, 5, 3, 0, 2};  
  SMatrix lhs(N,M,zero,v,r,c);


  std::vector<GFElem> rhs;
  rhs.push_back(GFElem(gf, 1)); rhs.push_back(GFElem(gf, 2)); rhs.push_back(GFElem(gf, 3));
  rhs.push_back(zero); rhs.push_back(GFElem(gf, 4)); rhs.push_back(GFElem(gf, 5));
  /* 1 
     2 
     3 
     0 
     4 
     5 */

  std::vector<GFElem> expected;
  expected.push_back(GFElem(gf, 15)); expected.push_back(zero); expected.push_back(zero); 
  expected.push_back(GFElem(gf, 7)); expected.push_back(GFElem(gf, 5)); 
  /* 15, 
     0, 
     0, 
     7,
     5  */

  std::vector<GFElem> result = lhs*rhs;
  BOOST_REQUIRE(expected.size() == result.size());
  for (unsigned i = 0; i < expected.size(); ++i)
    BOOST_CHECK_MESSAGE(result[i] == expected[i], "fail on i = " << i 
			<< ": expected " << expected[i] << ", result " << result[i]);
}

BOOST_AUTO_TEST_CASE(vector_ops_emulv) {
  // static std::vector<GFElem> emulv(const GFElem & lhs, const std::vector<GFElem> & rhs);
  GF gf(4, 19);
  std::vector<GFElem> vec = {GFElem(gf, 1),GFElem(gf, 6),GFElem(gf, 3),GFElem(gf, 7),GFElem(gf, 5)};
  /* 1 6 3 7 5 */
  std::vector<GFElem> res = SMatrix::emulv(GFElem(gf, 3), vec);
  std::vector<GFElem> exp = {GFElem(gf, 3),GFElem(gf, 10),GFElem(gf, 5),GFElem(gf, 9),GFElem(gf, 15)};
  /* 3 10 5 9 15 */

  BOOST_REQUIRE(exp.size() == res.size());
  for (unsigned i = 0; i < res.size(); ++i)
    BOOST_CHECK_MESSAGE(res[i] == exp[i], "fail on i = " << i 
			<< ": exp " << exp[i] << ", vec " << res[i]);
}

BOOST_AUTO_TEST_CASE(vector_ops_add) {
  // static std::vector<GFElem> & add(std::vector<GFElem> & acc, const std::vector<GFElem> & b);
  // static std::vector<GFElem> & add(const std::vector<GFElem> & a, const std::vector<GFElem> & b, std::vector<GFElem> & result);
  GF gf(4, 19);
  std::vector<GFElem> lhs = {GFElem(gf, 1),GFElem(gf, 6),GFElem(gf, 3),GFElem(gf, 7),GFElem(gf, 5)};
  /* 1 6 3 7 5 */
  std::vector<GFElem> rhs = {GFElem(gf, 2),GFElem(gf, 6),GFElem(gf, 6),GFElem(gf, 8),GFElem(gf, 0)};
  /* 2 6 6 8 0 */
  std::vector<GFElem> result;

  SMatrix::add(lhs, rhs, result);
  std::vector<GFElem> exp = {GFElem(gf, 3),GFElem(gf, 0),GFElem(gf, 5),GFElem(gf, 15),GFElem(gf, 5)};
  /* 3 0 5 15 5 */

  BOOST_REQUIRE(exp.size() == result.size());
  for (unsigned i = 0; i < result.size(); ++i)
    BOOST_CHECK_MESSAGE(result[i] == exp[i], "fail on i = " << i 
			<< ": exp " << exp[i] << ", result " << result[i]);

  SMatrix::add(lhs, rhs);
  BOOST_REQUIRE(exp.size() == lhs.size());
  for (unsigned i = 0; i < lhs.size(); ++i)
    BOOST_CHECK_MESSAGE(lhs[i] == exp[i], "fail on i = " << i 
			<< ": exp " << exp[i] << ", lhs " << lhs[i]);
}

BOOST_AUTO_TEST_CASE(vector_ops_other) {
  // static bool equals(const std::vector<GFElem> & a, const std::vector<GFElem> & b);
  // static std::vector<GFElem> gen_random_GF_vector(const GF & field, unsigned n); // Используется стандартный генератор.
  Gen gen;
  GF gf(4, 19);
  const unsigned N = 5;
  std::vector<GFElem> one;
  std::vector<GFElem> two = {GFElem(gf, 3),GFElem(gf, 0),GFElem(gf, 5),GFElem(gf, 15),GFElem(gf, 5)};
  std::vector<GFElem> three = {GFElem(gf, 3),GFElem(gf, 0),GFElem(gf, 5),GFElem(gf, 15),GFElem(gf, 5)};

  BOOST_CHECK(!SMatrix::equals(one, two));
  BOOST_CHECK(SMatrix::equals(two, three));
  three[3].Set(2);
  BOOST_CHECK(!SMatrix::equals(two, three));

  std::vector<GFElem> random;
  for (unsigned i = 0; i < N; ++i) {
    random = SMatrix::gen_random_GF_vector(gen, gf, 5);
    for(auto && el : random)
      std::cout << el << " ";
    std::cout << std::endl;
  }
}

BOOST_AUTO_TEST_CASE(dprod_test) {
  GF gf(4,19);
  GFElem zero(gf, 0);

  std::vector<GFElem> lhs;
  lhs.push_back(GFElem(gf, 6)); lhs.push_back(GFElem(gf, 5)); lhs.push_back(zero);
  lhs.push_back(GFElem(gf, 4)); lhs.push_back(GFElem(gf, 3)); lhs.push_back(GFElem(gf, 2));
  /* 6 5 0 4 3 2 */

  std::vector<GFElem> rhs;
  rhs.push_back(GFElem(gf, 1)); rhs.push_back(GFElem(gf, 2)); rhs.push_back(GFElem(gf, 3));
  rhs.push_back(zero); rhs.push_back(GFElem(gf, 4)); rhs.push_back(GFElem(gf, 5));
  /* 1 2 3 0 4 5 */

  GFElem result(gf, 10);
  GFElem el(gf, 0);
  GFElem el1(gf, 0);
  SMatrix::dprod(lhs, rhs, el);
  SMatrix::dprod(lhs, rhs, el1);
  BOOST_CHECK(el == result);
  BOOST_CHECK(el1 == result);
}

BOOST_AUTO_TEST_SUITE_END()

// matrix solve
BOOST_AUTO_TEST_SUITE(CG_method_test)

BOOST_AUTO_TEST_CASE(little) {
}

BOOST_AUTO_TEST_CASE(small) {
  /* GF(2^5). Primitive = x^5 + x^3 + 1.
  /* Исходная матрица.     Вектор b.  После л. тр. Гаусса.  Вектор b. Вектор x0.
     0,1,0,0,0,1,0,0,0,1      0       1,1,0,0,1,0,1,1,0,0      0      a^3+a^2+1 (13)
     0,0,1,0,1,1,0,0,1,0      1       1,1,1,0,1,0,0,0,1,0      1      a^2+1     (5)
     0,0,0,0,0,1,0,1,0,0      0       0,1,1,0,1,1,1,1,0,1      1      a^2       (4)
     1,1,1,0,0,0,1,1,1,1      1       0,0,0,1,1,0,0,0,1,0      0      1         (1)
     1,0,0,0,0,0,0,1,1,1      0       1,1,1,1,0,0,1,0,0,0      0      a^2       (4)
     1,0,1,0,0,0,0,0,0,0      1       0,0,1,0,0,0,1,0,1,1      1      a+1       (3)
     1,0,0,0,0,0,0,0,0,0      0       1,0,1,0,1,1,0,0,1,1      0      a^2       (4)
     0,1,0,0,1,1,1,1,0,0      0       1,0,1,0,0,0,0,1,0,0      1      a^2       (4)
     1,0,0,0,1,0,0,1,0,0      0       0,1,0,1,0,1,1,0,0,0      0      a^2       (4)
     0,0,0,1,1,0,0,0,1,0      0       0,0,1,0,0,1,1,0,0,1      0      1         (1).  */

  /* Expected result: vector[column] - [[0],[0],[0],[1],[0],[0],[1],[1],[1],[1]]. */

  GF gf(5, 41);
  GFElem zero(gf, 0);
  GFElem one(gf, 1);

  // unsigned N2 = 10, M2 = 10;
  // std::vector<GFElem> v2(46, one);
  // std::vector<unsigned> r2 = {0,5,10,17,20,25,29,35,38,42,46};
  // std::vector<unsigned> c2 = { 0,1,4,6,7, 
  // 			      0,1,2,4,8, 
  // 			      1,2,4,5,6,7,9, 
  // 			      3,4,8,
  // 			      0,1,2,3,6,
  // 			      2,6,8,9,
  // 			      0,2,4,5,8,9,
  // 			      0,2,7,
  // 			      1,3,5,6,
  // 			      2,5,6,9 };
  // SMatrix A2(N2, M2, zero, v2, r2, c2);
  // A2.printData();
  // std::vector<GFElem> b = { zero, one, one, zero, zero, one, zero, one, zero, zero };

  unsigned N = 10, M = 10;
  std::vector<GFElem> v(34, one);
  std::vector<unsigned> r = {0,3,7,9,16,20,22,23,28,31,34};
  std::vector<unsigned> c = {1,5,9, 
			     2,4,5,8, 
			     5,7, 
			     0,1,2,6,7,8,9, 
			     0,7,8,9, 
			     0,2, 
			     0, 
			     1,4,5,6,7, 
			     0,4,7, 
			     3,4,8};
  SMatrix A(N, M, zero, v, r, c);
  // A.printData();
  SMatrix At(A.transpose());
  // At.printData();
  // (At*A).printData();
  

  std::vector<GFElem> x0 = {GFElem(gf, 13), GFElem(gf,  5), GFElem(gf,  4), GFElem(gf,  1), GFElem(gf,  4), 
			    GFElem(gf,  3), GFElem(gf,  4), GFElem(gf,  4), GFElem(gf,  4), GFElem(gf,  1) };
  std::vector<GFElem> b = { one, one, one, zero, one, zero, zero, zero, one, zero };
  std::vector<GFElem> expected = { zero, zero, zero, one, zero, zero, one, one, one, one };

  int flag = 0;
  std::vector<GFElem> result = SMatrix::cg_method(A, b, x0, flag);

  BOOST_REQUIRE(expected.size() == result.size());
  for (unsigned i = 0; i < result.size(); ++i)
    BOOST_CHECK_MESSAGE(result[i] == expected[i], "fail on i = " << i 
			<< ": expected " << expected[i] << ", result " << result[i]);
}

BOOST_AUTO_TEST_CASE(medium) {
}

BOOST_AUTO_TEST_CASE(large) {
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(solve_sparse_test)

BOOST_AUTO_TEST_CASE(small) {
  GF gf(5, 41);
  Gen gen;
  GFElem zero(gf, 0);
  GFElem one(gf, 1);
  unsigned N = 10, M = 10;
  std::vector<GFElem> v(34, one);
  std::vector<unsigned> r = {0,3,7,9,16,20,22,23,28,31,34};
  std::vector<unsigned> c = {1,5,9, 
			     2,4,5,8, 
			     5,7, 
			     0,1,2,6,7,8,9, 
			     0,7,8,9, 
			     0,2, 
			     0, 
			     1,4,5,6,7, 
			     0,4,7, 
			     3,4,8};
  SMatrix A(N, M, zero, v, r, c);
  for (unsigned column = 0; column < A.getM(); ++column) {
    std::vector<GFElem> b(A.getColumn(column));
    
    std::vector<GFElem> result = SMatrix::solve_sparse(A, b, gen);

    std::cout << "b ";
    for (auto && el : b)
      std::cout << el << " ";
    std::cout << std::endl;
    std::cout << "result ";
    for (auto && el : result)
      std::cout << el << " ";
    std::cout << std::endl;

    std::vector<GFElem> b1(A*result);
    BOOST_REQUIRE(b1.size() == b.size());
    for (unsigned i = 0; i < b1.size(); ++i)
      BOOST_CHECK_MESSAGE(b1[i] == b[i], "fail on i = " << i 
			  << ": b " << b[i] << ", b1 " << b1[i]);
  }
}

BOOST_AUTO_TEST_SUITE_END()
