#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Sieve_test
#include <boost/test/unit_test.hpp>
#include <ippcp.h>
#include <fstream>
#include "include/bignum.h"
#include "include/sieve.h"

// for private functions testing.
namespace sieve_test {
  
  struct testing {
    static BigNumber tonelli_shanks(const BigNumber & a, const BigNumber & p) {
      return Sieve::tonelli_shanks(a, p);
    }
    static BigNumber mod_sqrt_bf(const BigNumber & a, const BigNumber & p){
      return Sieve::mod_sqrt_bf(a, p);
    }
    static BigNumber shanks(const BigNumber & a, const BigNumber & p){
      return Sieve::shanks(a, p);
    }
    static unsigned factor_out(BigNumber & a, const BigNumber & factor){
      return Sieve::factor_out(a, factor);
    }
    static bool pdiv(const BigNumber & a, 
		     const std::vector<BigNumber> & factor_base,
		     std::vector<int> & exp){
      return Sieve::pdiv(a, factor_base, exp);
    }
  };

}

using namespace sieve_test;

BOOST_AUTO_TEST_SUITE(sieve_utils)

BOOST_AUTO_TEST_CASE(creation) {
  Sieve created(BigNumber("123456789"));
}

BOOST_AUTO_TEST_CASE(factor_out) {
  BigNumber a(288);
  BigNumber factor(3);
  BOOST_CHECK(testing::factor_out(a, factor) == 2);
  BOOST_CHECK(a == BigNumber(32));

  BigNumber a1(-5);
  BigNumber factor1(-1);
  BOOST_CHECK(testing::factor_out(a1, factor1) == 1);
  BOOST_CHECK(a1 == BigNumber(5));
}

BOOST_AUTO_TEST_CASE(pdiv_little) {
  BigNumber num1(3300); // 2*2*3*5*5*11
  BigNumber num2(-1260); // -1*2*2*3*3*5*7
  std::vector<BigNumber> factor_base;
  factor_base.push_back(BigNumber(-1));
  factor_base.push_back(BigNumber(2));
  factor_base.push_back(BigNumber(3));
  factor_base.push_back(BigNumber(5));
  factor_base.push_back(BigNumber(11));

  std::vector<int> exponents;  
  BOOST_CHECK(testing::pdiv(num1, factor_base, exponents) == true);
  BOOST_CHECK(exponents[0] == 0); BOOST_CHECK(exponents[1] == 2);
  BOOST_CHECK(exponents[2] == 1); BOOST_CHECK(exponents[3] == 2);
  BOOST_CHECK(exponents[4] == 1);

  std::vector<int> exponents1;
  BOOST_CHECK(testing::pdiv(num2, factor_base, exponents1) == false);
  BOOST_CHECK(exponents1[0] == 1); BOOST_CHECK(exponents1[1] == 2);
  BOOST_CHECK(exponents1[2] == 2); BOOST_CHECK(exponents1[3] == 1);
  BOOST_CHECK(exponents1[4] == 0);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(mod_sqrt)

BOOST_AUTO_TEST_CASE(tonelli_shanks) {
  BigNumber s(testing::tonelli_shanks(BigNumber(6), BigNumber(29)));
  BOOST_CHECK(s == BigNumber(8) || s == BigNumber(21));
}

BOOST_AUTO_TEST_CASE(mod_sqrt_bf) {
  BigNumber s(testing::mod_sqrt_bf(BigNumber(6), BigNumber(29)));
  BOOST_CHECK(s == BigNumber(8) || s == BigNumber(21));
}

BOOST_AUTO_TEST_CASE(shanks) {
  
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(sieve_test)

BOOST_AUTO_TEST_CASE(gen_smooth_pdiv) {
  unsigned N;
  std::vector<BigNumber> primes;
  unsigned a;
  unsigned b;
  {
    std::ifstream in("test/tests/sieve_gen_smooth_pdiv.in");
    BOOST_REQUIRE(in.is_open());
    in >> N;
    for (int i = 0; i < N; ++i){
      unsigned tmp;
      in >> tmp;
      primes.push_back(BigNumber(tmp));
    }
    in >> a;
    in >> b;
    in.close();
    BOOST_REQUIRE(!in.is_open());
  }

  Sieve sieve(29);
  std::vector<BigNumber> smooth;
  std::vector<std::vector<int>> exps;

  sieve.gen_smooth_pdiv(N, a, b, primes, smooth, exps);
  
  std::ifstream in("test/tests/sieve_gen_smooth_pdiv.out");
  BOOST_REQUIRE(in.is_open());
  std::vector<BigNumber> answer;
  while (!in.eof()){
    unsigned tmp;
    in >> tmp;
    answer.push_back(BigNumber(tmp));
  }
  answer.erase(answer.end()-1);

  BOOST_REQUIRE(smooth.size() == answer.size());
  for(unsigned i = 0; i < answer.size(); ++i) {
    BOOST_CHECK_MESSAGE(smooth[i] == answer[i], 
			"index" << i << 
			"smooth: " << smooth[i] <<
			"answer: " << answer[i]);
  }
}

BOOST_AUTO_TEST_CASE(gen_smooth_qsieve_factor_out) {
  BigNumber N;
  BigNumber a;
  BigNumber b;
  std::vector<BigNumber> factors;
  {
    std::ifstream in("test/tests/sieve_gen_smooth_qsieve_factor_out.in");
    BOOST_REQUIRE(in.is_open());
    unsigned tmp;
    in >> tmp; N = BigNumber(tmp);
    in >> tmp; a = BigNumber(tmp);
    in >> tmp; b = BigNumber(tmp);
    unsigned fsize;
    in >> fsize;
    for (int i = 0; i < fsize; ++i) {
      int factor;
      in >> factor;
      factors.push_back(BigNumber(factor));
    }
    in.close();
    BOOST_REQUIRE(!in.is_open());
  }
  
  // std::cout << N 
  // 	    << a << b << std::endl;
  // for(auto && el : factors)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  Sieve s(N);
  std::vector<BigNumber> smooth;
  s.gen_smooth_qsieve_factor_out(BigNumber((unsigned)factors.size()), a, b, factors, smooth);

  // for(auto && el : smooth)
  //   std::cout << el << std::endl;

  BOOST_CHECK(!smooth.empty());

  std::vector<int> exp;
  for(auto && el : smooth)
    BOOST_CHECK(testing::pdiv(el*el - N, factors, exp));

  {
    std::ofstream out("test/gen_smooth_qsieve_factor_out.a");
    BOOST_REQUIRE(out.is_open());
    for(auto && sm : smooth)
      out << sm << " ";
    out << std::endl;
    out.close();
    BOOST_REQUIRE(!out.is_open());
  }
}

BOOST_AUTO_TEST_CASE(gen_smooth_qsieve_log) {
  BigNumber N;
  BigNumber a;
  BigNumber b;
  std::vector<BigNumber> factors;
  {
    std::ifstream in("test/tests/sieve_gen_smooth_qsieve_log.in");
    BOOST_REQUIRE(in.is_open());
    unsigned tmp;
    in >> tmp; N = BigNumber(tmp);
    in >> tmp; a = BigNumber(tmp);
    in >> tmp; b = BigNumber(tmp);
    unsigned fsize;
    in >> fsize;
    for (int i = 0; i < fsize; ++i) {
      int factor;
      in >> factor;
      factors.push_back(BigNumber(factor));
    }
    in.close();
    BOOST_REQUIRE(!in.is_open());
  }
  
  // std::cout << N 
  // 	    << a << b << std::endl;
  // for(auto && el : factors)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  Sieve s(N);
  std::vector<BigNumber> smooth;
  std::vector<std::vector<int>> exponents;
    s.gen_smooth_qsieve_log(BigNumber((unsigned)factors.size()), 
			    a, b, factors, smooth, exponents);

  // for(auto && el : smooth)
  //   std::cout << el << std::endl;

  BOOST_CHECK(!smooth.empty());
  BOOST_CHECK(smooth.size() == exponents.size());

  std::vector<int> exp;
  for(unsigned i = 0; i < smooth.size(); ++i) {
    BOOST_CHECK(testing::pdiv(smooth[i]*smooth[i] - N, factors, exp));
    for(unsigned j = 0; j < factors.size(); ++j) {
      BOOST_CHECK(exp[j] == exponents[i][j]);
    }
  }

  {
    std::ofstream out("test/gen_smooth_qsieve_log.a");
    BOOST_REQUIRE(out.is_open());
    for(auto && sm : smooth)
      out << sm << " ";
    out << std::endl;
    for(std::vector<int> vec : exponents) {
      for(auto && e : vec)
	out << e << " ";
      out << std::endl;
    }
    out.close();
    BOOST_REQUIRE(!out.is_open());
  }  
}

BOOST_AUTO_TEST_SUITE_END()


