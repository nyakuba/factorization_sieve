#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE GenBN_test
#include <boost/test/unit_test.hpp>
#include <ippcp.h>
#include "include/bignum.h"
#include "include/rand.h"

BOOST_AUTO_TEST_SUITE(genbn_test)

BOOST_AUTO_TEST_CASE(prime_gen) {
  Gen generator;
  const int bitsize = 64;
  const unsigned N = 5;
  BigNumber fst, snd;
  unsigned i = 0;
  while (i < N) {
    generator.gen_BN(bitsize, fst);
    generator.gen_BN(bitsize, snd);
    std::cout << fst << std::endl;
    std::cout << snd << std::endl;
    ++i;
  }
}

BOOST_AUTO_TEST_CASE(is_prime_test) {
  Gen generator;
  const unsigned tests_count = 50;

  BigNumber prime(19);
  BOOST_CHECK(generator.test_prime(tests_count, prime));

  // define known prime value (2^128 -3)/76439
  Ipp32u bnuPrime1[4] = {0xBEAD208B, 0x5E668076, 0x2ABF62E3, 0xDB7C};
  BigNumber prime1(bnuPrime1, 4);
  BOOST_CHECK(generator.test_prime(tests_count, prime1));

  // define another known prime value 2^128 -2^97 -1
  Ipp32u bnuPrime2[4] = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFD};
  BigNumber prime2(bnuPrime2, 4);
  BOOST_CHECK(generator.test_prime(tests_count, prime2));

  BigNumber composite(prime1*prime2);
  BOOST_CHECK(!generator.test_prime(tests_count, composite));
}

BOOST_AUTO_TEST_SUITE_END()
