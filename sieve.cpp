#include "include/sieve.h"

#define SHANKS_LOWER_BOUND 50;

Sieve::Sieve():
  m_N(BigNumber::One()), m_B(0), m_a(0), m_b(0), m_interval() {}

Sieve::Sieve(const BigNumber & N): 
  m_N(N), m_B(0), m_a(0), m_b(0), m_interval() {}
Sieve::~Sieve(){}

void Sieve::setN(BigNumber & N) {
  m_N = N;
}

void Sieve::get_smooth_exp(const GF & gf, const unsigned & B, 
		    const BigNumber & N, const std::vector<BigNumber> & fb, 
		    std::vector<BigNumber> & smooth, SMatrix & exponents) {
  smooth.clear();
  smooth.resize(B);
  BigNumber a(1);
  BigNumber b(N);
  b.bsqrt();
  // TODO: проследить за величиной промежутка
  std::vector<BigNumber> sm;
  std::vector<std::vector<int>> exps;
  gen_smooth_qsieve_log(BigNumber(B), a, b, fb, sm, exps);

  std::cout << "sm " << std::endl;
  for (auto && el : smooth)
    std::cout << el << " ";
  std::cout << std::endl;
  
  // TODO: эффективная реализация формирования матрицы
  for (unsigned i = 0; i < exps.size() && i < B; ++i)
    for (unsigned j = 0; j < exps[i].size(); ++j)
      if (exps[i][j] == 1)
	exponents.set(i,j,GFElem(gf, 1));
  unsigned sindex = 0;
  for (unsigned i = 0; i < sm.size() && i < B; ++i) {
    smooth[sindex] = sm[i];
    ++sindex;
  }
 
  while (sindex < B) {
    if (b > N) return;
    a += b;
    b += b;
    gen_smooth_qsieve_log(BigNumber(B), a, b, fb, sm, exps);
    for (unsigned i = 0; i < exps.size() && i+sindex < B; ++i)
      for (unsigned j = 0; j < exps[i].size(); ++j)
	if (exps[i][j] == 1)
	  exponents.set(i+sindex,j,GFElem(gf, 1));
    unsigned sindex = 0;
    for (unsigned i = 0; i < sm.size() && sindex < B; ++i) {
      smooth[sindex] = sm[i];
      ++sindex;
    }    
    std::cout << "smooth " << std::endl;
    for (auto && el : smooth)
      std::cout << el << " ";
    std::cout << std::endl;
  }
}

void Sieve::gen_smooth_pdiv(const BigNumber & B, 
			    const BigNumber & a, 
			    const BigNumber & b,
			    const std::vector<BigNumber> & factor_base, 
			    std::vector<BigNumber> & smooth,
			    std::vector<std::vector<int>> & exponents) {
  assert((b - a) < BigNumber(-1U));
  assert(a > BNZero);
  assert(b > BNZero);

  smooth.clear();
  exponents.clear();
  for (BigNumber num(a); num < BigNumber(b); num+=BNOne) {
    BigNumber s(num);
    std::vector<int> exp;
    for (auto && el : factor_base) {
      unsigned e = factor_out(s, el);
      exp.push_back(e);
    }
    if (s == BNOne) {
      smooth.push_back(num);
      exponents.push_back(exp);
    }
  }
}

void Sieve::gen_smooth_qsieve_factor_out(const BigNumber & B, const BigNumber & a, const BigNumber & b,
					 const std::vector<BigNumber> & factor_base, 
					 std::vector<BigNumber> & smooth){
  assert((b - a) < BigNumber(-1U));
  assert(a > BNZero);
  assert(b > BNZero);

  std::cout << "in gen_smooth_qsieve_factor_out" << std::endl;

  smooth.clear();

  // инициализация промежутка.
  unsigned rsize = (b - a).uns();
  std::vector<BigNumber> interval(rsize);
  for (unsigned index = 0; index < rsize; ++index){
    // std::cout << index << " " << std::endl;
    BigNumber elem(BigNumber(index) + a);
    elem.bpow(BNTwo);
    elem -= m_N;
    interval[index] = elem;
  }

  // ищем корни s^2 = N mod p^l
  // пока что только для l = 1.
  // TODO переделать типы factor.
  for (int i = 1; i < factor_base.size(); ++i) {
    // std::cout << "prime " << factor_base[i]  << std::endl;

    BigNumber res1 = tonelli_shanks(m_N, factor_base[i]);
    BigNumber res2 = factor_base[i].InverseMul(res1);
    // std::cout << "s" << res1 << " " << res2 << std::endl;

    BigNumber start_index((a/factor_base[i])*factor_base[i] - a);
    unsigned step = factor_base[i].uns();
    for (unsigned k = start_index.uns(); k < rsize; k+=step) {
      if(k + res1.uns() < rsize)
	factor_out(interval[k + res1.uns()], factor_base[i]);
      if(k + res2.uns() < rsize)
	factor_out(interval[k + res2.uns()], factor_base[i]);
    }
  }

  // собираем B-гладкие числа.
  for (unsigned i = 0; i < rsize; ++i){
    interval[i].babs();
    if(interval[i] == BNOne) {
      smooth.push_back(BigNumber(i) + a);
    }
  }

  // Просеиваем по степеням простых чисел.
  // if (BigNumber((unsigned)smooth.size()) < B) {
  //   for (int i = 1; i < factor_base.size(); ++i) {
  //     BigNumber p = factor_base[i];
  //     BigNumber bp(m_N); bp.bsqrt();
  //     while (p < bp) {
  // 	bp *= p;
  // 	std::cout << "p " << p  << std::endl;
  // 	std::vector<BigNumber> res(bruteforce_q(m_N, p));
	
  // 	BigNumber start_index((a/p)*p - a);
  // 	unsigned step = p.uns();
  // 	for (auto && r : res) {
  // 	  for (unsigned k = start_index.uns(); k < rsize; k+=step) {
  // 	    if(k + r.uns() < rsize)
  // 	      factor_out(interval[k + r.uns()], factor_base[i]);
  // 	  }
  // 	}
  //     }
  //   }
  // }
}

std::vector<BigNumber> Sieve::bruteforce_q(const BigNumber & a, const BigNumber & p) {
  std::vector<BigNumber> results;
  BigNumber e(a % p);
  for (BigNumber i = BigNumber(1); i < p; i+=1) {
    BigNumber el(i); el.bpow(BigNumber(2));
    if (el == e)
      results.push_back(i);
  }
  return results;
}

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void Sieve::gen_smooth_mp(const int & B, const std::vector<int> & fb,
			  const std::vector<BigNumber> & tsh_fb,
			  const BigNumber pa, Gen & gen,
			  std::vector<BigNumber> & smooth,
			  std::vector<BigNumber> & acoeff,
			  std::vector<std::vector<int>> & exponents) const {

  BigNumber N(m_N);
  BigNumber p(pa);
  BigNumber a(pa); a*=pa;
  // std::cout << "a " << a << std::endl;

  BigNumber sieve_size(2*N); sieve_size.bsqrt();
  sieve_size /= a;
  // std::cout << "sieve_size " << sieve_size << std::endl;

  unsigned M = 0;
  if (sieve_size.BitSize() > sizeof(unsigned)*8)
    M = Sieve::MAX_SIEVE_INTERVAL;
  else {
    M = sieve_size.uns();
    if (M > Sieve::MAX_SIEVE_INTERVAL)
      M = Sieve::MAX_SIEVE_INTERVAL;
  }
  // std::cout << "M " << std::dec << M << " " << 2*M << std::endl;

  // pthread_mutex_lock(&mutex);
  BigNumber tmp(tonelli_shanks(N, p));
  // pthread_mutex_unlock(&mutex);

  BigNumber tmp1(BNZero); tmp1-=tmp; tmp1%=p;
  if (tmp1 < tmp) {
    tmp = tmp1;
  }
  // std::cout << "tmp " << tmp << std::endl;

  BigNumber t(2*tmp);
  // BigNumber t1((BNZero-((tmp*tmp - m_N)/p)) % p); 
  BigNumber t1(BNZero);
  BigNumber t1_tmp(tmp); t1_tmp*=tmp; t1_tmp-=N; t1_tmp/=p;
  t1-=t1_tmp; t1%=p;

  // std::cout << t << t1 << std::endl;
  // std::cout << p.InverseMul(t) << std::endl;

  // BigNumber b = (tmp + t1*p.InverseMul(t)*p) % a;
  BigNumber b(t1); b*=p.InverseMul(t); b*=p; b+=tmp; b%=a;

  // std::cout << "a " << a << std::endl;
  // std::cout << "b " << b << std::endl;
  
  double * sieve = nullptr;
  sieve = new double[2*M];
  memset(sieve, 0, 2*M*sizeof(double));
  // std::cout << "M " << M << std::endl;

  // for (auto && el : tsh_fb)
  //   std::cout << el << std::endl;

  for (unsigned i = 1; i < B; ++i) {
    BigNumber factor(fb[i]);

    // pthread_mutex_lock(&mutex);
    // BigNumber t(tonelli_shanks(m_N, factor));
    // pthread_mutex_unlock(&mutex);
    BigNumber t(tsh_fb[i]);
    // std::cout << t << std::endl;


    BigNumber inva(factor.InverseMul(a));
    // BigNumber r((BNZero - b + t)*inva % factor);
    BigNumber r(BNZero); r-=b; r+=t; r%=factor; r*=inva; r%=factor;
    // BigNumber s((BNZero - b - t)*inva % factor);
    BigNumber s(BNZero); s-=b; s-=t; s%=factor; s*=inva; s%=factor;
    double logp = log(fb[i]);
    // std::cout << "r " << r << ", s " << s << ", logp " << logp << std::endl;
    
    int start = -(M/fb[i])*fb[i];
    // std::cout << "prime " << fb[i] << " start index " << start << std::endl;

    int index = start;
    int ru = r.uns(); int su = s.uns();
    // std::cout << "ru " << ru << ", su " << su << ", index " << index << std::endl;
    int bm = M;
    while (index < bm) {
      if (index + ru < bm) {
 	sieve[index + ru + bm] = sieve[index + ru + bm] + logp;
      }
      if (index + su < bm) {
 	sieve[index + su + bm] = sieve[index + su + bm] + logp;
      }
      index = index + fb[i];
    }
  }

  // std::cout << "sieve array:" << std::endl;
  // for (int i = 0; i < 2*M; ++i)
  //   std::cout << sieve[i] << " ";
  // std::cout << std::endl;

  double loga = loge(a);
  // std::cout << "loga " << loga << std::endl;

  BigNumber invA(N.InverseMul(a));
  for (int i = 0; i < 2*M; ++i) {
    if (sieve[i] != 0) {
      int x = i - (int) M;
      // BigNumber axb(x*a+b);
      BigNumber axb(x); axb*=a; axb+=b;
      // BigNumber val(axb*axb - m_N);
      BigNumber val(axb); val*=axb; val-=N;
      // std::cout << x << " " << val << std::endl;
      bool negative = false;
      if (val < BNZero) {
	val = BNZero - val;
	negative = true;
      }
      double l = loge(val) - loga;
      // if (i == M-26) {
      // 	std::cout << val << std::endl;
      // 	std::cout << x << " " <<  l << std::endl;
      // 	std::cout << axb << std::endl;
      // 	std::cout << a << " " << b << std::endl;
      // 	std::cout << p << std::endl;
      // }
      // TODO: Можно варьировать границу, допуская большее количество кандидатов.
      if (abs(l - sieve[i]) < 2) {
	// std::cout << "x = " << x << ", log(Q(x)) - log(a) = " << l << ", sieve[i] = " << sieve[i] << std::endl;
	std::vector<int> e;
	if (pdiv(val/a, fb, e)) {
	  // std::cout << "+1 " << std::flush;
	  // std::cout << pa << " ";
	  // std::cout << "found!" << std::endl;
	  smooth.push_back(axb);
	  acoeff.push_back(pa);
	  if (negative) e[0] = 1;
	  exponents.push_back(e);
	  // std::cout << "sm: " << axb << " val: " << val << " ";
	  // std::cout << "e: ";
	  // for(auto && el : e)
	  //   std::cout << el << " ";
	  // std::cout << std::endl;
	}
      }
    }
  }
  // std::cout << std::endl;
  delete sieve;
}

void Sieve::gen_smooth_qsieve_log(const BigNumber & B, const BigNumber & a, const BigNumber & b,
				  const std::vector<BigNumber> & factor_base, 
				  std::vector<BigNumber> & smooth,
				  std::vector<std::vector<int>> & exponents) {
  assert((b - a) < BigNumber(-1U));
  assert(a > BNZero);
  assert(b > BNZero);

  smooth.clear();

  // инициализация промежутка.
  unsigned rsize = (b - a).uns();
  std::vector<BigNumber> interval(rsize);
  for (unsigned index = 0; index < rsize; ++index){
    BigNumber elem(BigNumber(index) + a);
    elem.bpow(BNTwo);
    elem -= m_N; elem.babs();
    interval[index] = BigNumber((unsigned)loge(elem));
  }
  // пока обрабатываем только p^l, l=1.
  // В промежутке содержатся логарифмы. Вычитаем loge(p).
  // Элементы, близкие к 0 наиболее вероятно - B-гладкие.
  for (int i = 1; i < factor_base.size(); ++i) {
    BigNumber factor(factor_base[i]);
    std::cout << "factor " << factor << std::endl;
    BigNumber res1 = tonelli_shanks(m_N, factor);
    BigNumber res2 = factor.InverseMul(res1);
    std::cout << "s" << res1 << " " << res2 << std::endl;
    BigNumber loge_p = BigNumber((unsigned)loge(factor));

    BigNumber start_index((a/factor)*factor - a);
    unsigned step = factor.uns();
    for (unsigned k = start_index.uns(); k < rsize; k+=step) {
      if(k + res1.uns() < rsize)
	interval[k + res1.uns()] -= loge_p;
      if(k + res2.uns() < rsize)
	interval[k + res2.uns()] -= loge_p;
    }
  }
  // собираем B-гладкие числа.
  // верхняя граница проверки - пусть логарифм наибольшего фактора.
  BigNumber upper_bound = BigNumber((unsigned)loge(*(factor_base.end()-1)));
  for (unsigned i = 0; i < rsize; ++i){
    if(interval[i] <= upper_bound) {
      BigNumber candidate(BigNumber(i) + a);
      candidate.bpow(BNTwo);
      candidate -= m_N;
      std::cout << "candidate " << candidate << std::endl;
      // проверяем кандидата пробным делением.
      // можно тут же построить его вектор экспонент.
      std::vector<int> exp;
      if(pdiv(candidate, factor_base, exp)) {
	smooth.push_back(BigNumber(i) + a);
	exponents.push_back(exp);
      }
    }
  }
}

bool Sieve::pdiv(const BigNumber & a, 
		 const std::vector<BigNumber> & factor_base, 
		 std::vector<int> & exp){
  exp.clear();
  exp.resize(factor_base.size());
  BigNumber s(a);
  if (s < BNZero) {
    exp[0] = 1;
    s.babs();
  }
  for(unsigned i = 1; i < factor_base.size(); ++i) {
    exp[i] = factor_out(s, factor_base[i]);
  }
  return s == BNOne;
}

bool Sieve::pdiv(const BigNumber & a, 
		 const std::vector<int> & factor_base, 
		 std::vector<int> & exp){
  exp.clear();
  exp.resize(factor_base.size());
  BigNumber s(a);
  if (s < BNZero) {
    exp[0] = 1;
    s = BNZero - s;
  }
  for(unsigned i = 1; i < factor_base.size(); ++i) {
    exp[i] = factor_out(s, BigNumber(factor_base[i]));
  }
  return s == BNOne;
}

BigNumber Sieve::tonelli_shanks(const BigNumber & a, const BigNumber & p) {
  BigNumber q = p - BNOne;
  BigNumber s;
  while(q.IsEven()){
    q /= BNTwo;
    s += BNOne;
  }
  BigNumber w;
  for (BigNumber k(1); k < p; k += BNOne) {
    if (p.ModPow(k, (p - BNOne) / BNTwo) == p.Modulo(BigNumber(-1))) {
      w = k;
      break;
    }
  }
  BigNumber v(p.ModPow(w, q));
  BigNumber a1(p.InverseMul(a));
  BigNumber r(p.ModPow(a, (q + BNOne) / BNTwo));
  while (true) {
    BigNumber i;
    BigNumber t(r); t*=r;
    t = p.Modulo(t * a1);
    while (i < s) {
      if (t == BNOne) {
	break;
      } else {
	t *= t; t %= p;
	i += BNOne;
      }
    }
    if (i == BNZero){
      return r;
    } else {
      BigNumber pow(BNTwo);
      pow.bpow(s - i - BNOne);
      BigNumber vtmp(v); 
      vtmp.bpow(pow);
      // r = p.Modulo(r*vtmp);
      r *= vtmp; r %= p;
    }
    // std::cout << i << std::endl;
  }
}

BigNumber Sieve::shanks(const BigNumber & a, const BigNumber & p){
  // If p is small, we can easily find a solution by bf.
  if (p < BigNumber(50))
    return Sieve::mod_sqrt_bf(a, p);

  BigNumber u(p);
  u.bsqrt();
  std::vector<BigNumber> table;
  for (BigNumber i(BNZero); i < u-BNOne; i+=1) {

  }
  return BNZero;
}

// brute force
BigNumber Sieve::mod_sqrt_bf(const BigNumber & a, const BigNumber & p){
  BigNumber s(1);
  while (s <= p) {
    if (p.ModPow(s, BNTwo) == a) {
      return s;
    } else {
      s += BNOne;
    }
  }
  return s;
}

unsigned Sieve::factor_out(BigNumber & a, const BigNumber & factor){
  unsigned power = 0;
  while (a % factor == BNZero) {
    a /= factor;
    ++power;
  }
  return power;
}
