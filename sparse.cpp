#include "include/sparse.h"

SMatrix::SMatrix(int N, int M, const GFElem & zero):
  m_N(N), m_M(M), m_zero(zero), m_r(N+1, 0) {}

SMatrix::SMatrix(int N, int M, const GFElem & zero,
	const std::vector<GFElem> & v, 
	const std::vector<unsigned> & r, 
	const std::vector<unsigned> & c):
  m_N(N), m_M(M), m_zero(zero), m_v(v), m_r(r), m_c(c) {}

SMatrix::SMatrix(const SMatrix & m):
  m_N(m.m_N), m_M(m.m_M), m_zero(m.m_zero), m_v(m.m_v), m_r(m.m_r), m_c(m.m_c) {}

SMatrix::~SMatrix() {}

void SMatrix::setN(unsigned n) {
  m_N = n;
  m_r.resize(n+1);
}

void SMatrix::append(const std::vector<int> & column) {
  for (unsigned i = 0; i < column.size(); ++i) {
    if (column[i] != 0) {
      m_v.insert(m_v.begin()+m_r[i+1], GFElem(*m_zero.getGF(), column[i]));
      m_c.insert(m_c.begin()+m_r[i+1], m_M);
      for (unsigned j = i+1; j < m_r.size(); ++j)
	++m_r[j];
    }
  }
  ++m_M;
}

unsigned SMatrix::getN() const {
  return m_N;
}

unsigned SMatrix::getM() const {
  return m_M;
}

GFElem SMatrix::get(int row, int column) const {
  if (m_r[row+1] - m_r[row] != 0) {
    for (unsigned i = m_r[row]; 
	 i < m_r[row+1] && m_c[i] <= column; 
	 ++i) {
      if (m_c[i] == column)
	return m_v[i];
    }
  }
  return m_zero;
}

std::vector<GFElem> SMatrix::getRow(unsigned n) const {
  std::vector<GFElem> r(m_M, m_zero);
  if (m_r[n+1] - m_r[n] != 0) {
    unsigned vindex = m_r[n];
    for (unsigned col = 0; col < m_M; ++col) {
      if (vindex >= m_r[n+1])
	break;
      if (m_c[vindex] == col) {
	r[col] = m_v[vindex];
	++vindex;
      }
    }
  }
  return r;
}

std::vector<GFElem> SMatrix::getColumn(unsigned m) const {
  std::vector<GFElem> c(m_N, m_zero);
  for (unsigned row = 0; row < m_N; ++row) {
    if (m_r[row+1] - m_r[row] != 0) {
      for (unsigned i = m_r[row]; i < m_r[row+1] && m_c[i] <= m; ++i) {
	if (m_c[i] == m) {
	  c[row] = m_v[i];
	  break;
	}
      }
    }
  }
  return c;
}

bool SMatrix::filter(std::vector<BigNumber> & smooth, std::vector<BigNumber> & acoeff, std::vector<int> & fb) {
  bool flag = false;
  int rindex = 0, cindex = 0;
  for (int i = 0; i < m_r.size()-1; ++i) {
    int el_count = m_r[i+1]-m_r[i];
    if (el_count < 2) {
      if (el_count == 0) {
	rindex = i; cindex = m_M - 1;
      } else {
	rindex = i; cindex = m_c[m_r[i]];
      }
      flag = true;
      break;
    }
  }

  if (flag) {
    rm_rc(rindex, cindex);
    smooth.erase(smooth.begin()+cindex);
    acoeff.erase(acoeff.begin()+cindex);
    fb.erase(fb.begin()+rindex);
  }
  return flag;
}

void SMatrix::rm_rc(int row, int column) {
  for (int i = 0; i < m_r.size()-1; ++i) {
    for (int j = m_r[i]; j < m_r[i+1]; ++j) {
      if (m_c[j] == column) {
	for (int k = i+1; k < m_r.size(); ++k)
	  --m_r[k];
	m_v.erase(m_v.begin()+j);
	m_c.erase(m_c.begin()+j);
      }
    }
  }
  // this->printMatrix();
  // this->prinData();
  int el_count = m_r[row+1] - m_r[row];
  if (el_count != 0) {
    m_v.erase(m_v.begin()+m_r[row], m_v.begin()+m_r[row+1]);
    m_c.erase(m_c.begin()+m_r[row], m_c.begin()+m_r[row+1]);
  }
  for (int i = row+1; i < m_r.size(); ++i) {
    m_r[i]-=el_count;
  }
  m_r.erase(m_r.begin()+row);
  for (auto && el : m_c) {
    if (el > column)
      --el;
  }
  --m_N; --m_M;
}

GFElem SMatrix::zero() const {
  return m_zero;
}

void SMatrix::set(int row, int column, const GFElem & value) {
  if (row >= m_N || column >= m_M)
    throw std::invalid_argument("sparse set: index out of range.");
  // TODO if value == 0.
  if (value == m_zero) {
    for (unsigned i = m_r[row]; i < m_r[row+1]; ++i) {
      if (m_c[i] > column)
	break;
      if (m_c[i] == column) {
	m_v.erase(m_v.begin() + i);
	m_c.erase(m_c.begin() + i);
	for (unsigned j = row+1; j < m_r.size(); ++j)
	  --m_r[j];
      }
    }
  } else {
    for (unsigned i = m_r[row]; i < m_r[row+1]; ++i) {
      if (m_c[i] == column) {
	m_v[i] = value;
	return;
      } else if (m_c[i] > column) {
	m_v.insert(m_v.begin() + i, value);
	m_c.insert(m_c.begin() + i, column);
	for (unsigned j = row + 1; j < m_r.size(); ++j)
	  ++m_r[j];
	// if (row == 5 && column == 4){
	// 	for (auto && el : m_v)
	// 	  std::cout << el << " ";
	// 	std::cout << std::endl;
	// 	for (auto && el : m_r)
	// 	  std::cout << el << " ";
	// 	std::cout << std::endl;
	// 	for (auto && el : m_c)
	// 	  std::cout << el << " ";
	// 	std::cout << std::endl;
	// }
	return;
      }
    }
    m_v.insert(m_v.begin()+m_r[row+1], value);
    m_c.insert(m_c.begin()+m_r[row+1], column);
    for (unsigned j = row + 1; j < m_r.size(); ++j)
      ++m_r[j];
  }
}

void SMatrix::set(int N, int M, const GFElem & zero,
		  const std::vector<GFElem> & v, 
		  const std::vector<unsigned> & r, 
		  const std::vector<unsigned> & c) {
  m_N = N; m_M = M; m_zero = zero;
  m_v = v; m_r = r; m_c = c;
}

void SMatrix::set(const SMatrix & m) {
  m_N = m.m_N; m_M = m.m_M; m_zero = m.m_zero;
  m_v = m.m_v; m_r = m.m_r; m_c = m.m_c;
}

void SMatrix::add_col(unsigned col1, unsigned col2) {
  if (!empty()) {
    for (unsigned row = 0; row < m_N; ++row) {
      bool found1 = false, found2 = false;
      unsigned index1 = 0, index2 = 0;
      for (unsigned i = m_r[row]; i < m_r[row+1]; ++i) {
	if (m_c[i] == col1) {
	  index1 = i;
	  found1 = true;
	}
	if (m_c[i] == col2) {
	  index2 = i;
	  found2 = true;
	}
      }
      if (found1 && found2) {
	m_v[index1] += m_v[index2];
      } else if (found2) {
	set(row, col1, m_v[index2]);
      }
    }
  }
}

SMatrix operator+(const SMatrix & a, const SMatrix & b) {
  if (a.m_N != b.m_N || a.m_M != b.m_M)
    throw std::invalid_argument("sparse add: wrong dimentions.");
  std::vector<GFElem> v;
  std::vector<unsigned> r;
  std::vector<unsigned> c;
  r.push_back(0);
  for (unsigned row = 0; row < a.m_r.size() - 1; ++row) {
    unsigned aindex = a.m_r[row];
    unsigned bindex = b.m_r[row];
    unsigned a_end = a.m_r[row+1];    
    unsigned b_end = b.m_r[row+1];
    unsigned rcount = 0;
    while (aindex < a_end || bindex < b_end) {
      if (bindex >= b_end) { // В строке матрицы b нет больше элементов.
	v.push_back(a.m_v[aindex]);
	c.push_back(a.m_c[aindex]);
	++aindex;
      } else if (aindex >= a_end) { // В строке матрицы а нет больше элементов.
	v.push_back(b.m_v[bindex]);
	c.push_back(b.m_c[bindex]);
	++bindex;
      } else if (a.m_c[aindex] == b.m_c[bindex]) { // Элементы есть и в а, и в b, причем в одном столбце.
	v.push_back(a.m_v[aindex] + b.m_v[bindex]);
	c.push_back(a.m_c[aindex]);
	++aindex; ++bindex;
      } else if (a.m_c[aindex] < b.m_c[bindex]) { // Столбец элемента а идет раньше столбца элемента b.
	v.push_back(a.m_v[aindex]);
	c.push_back(a.m_c[aindex]);
	++aindex;
      } else { // Столбец элемента b идет раньше столбца элемента a.
	v.push_back(b.m_v[bindex]);
	c.push_back(b.m_c[bindex]);
	++bindex;
      }
      ++rcount;
    }
    r.push_back(*(r.end()-1) + rcount);
  }
  return SMatrix(a.m_N, a.m_M, a.m_zero, v, r, c);
}

SMatrix & SMatrix::operator*=(const GFElem & b) {
  for (auto && el : m_v)
    el *= b;
  return *this;
}

SMatrix operator*(const GFElem & lhs, const SMatrix & rhs) {
  SMatrix m(rhs);
  return m*=lhs;
}

std::vector<GFElem> SMatrix::emulv(const GFElem & lhs, const std::vector<GFElem> & rhs) {
  std::vector<GFElem> ret(rhs);
  for (auto && el : ret) 
    el *= lhs;
  return ret;
}

SMatrix operator*(const SMatrix & a, const SMatrix & b) {
  if (a.m_M != b.m_N)
    throw std::invalid_argument("sparse mmul: wrong dimentions.");
  std::vector<GFElem> v;
  std::vector<unsigned> r;
  std::vector<unsigned> c;
  r.push_back(0);
  for (unsigned row = 0; row < a.m_r.size()-1; ++row) {
    if (a.m_r[row+1] - a.m_r[row] == 0) {
      r.push_back(*(r.end()-1));
    } else {
      unsigned rcount = 0;
      for (unsigned column = 0; column < b.m_M; ++column) {
	GFElem acc(a.m_zero);
	// std::cout << "row " << row << " col " << column << std::endl;
	for (unsigned i = a.m_r[row]; i < a.m_r[row+1]; ++i){
	  unsigned acol = a.m_c[i];
	  bool found = false;
	  unsigned bindex = 0;
	  for (unsigned j = b.m_r[acol]; j < b.m_r[acol+1]; ++j) {
	    if (b.m_c[j] > column)
	      break;
	    if (b.m_c[j] == column) {
	      found = true;
	      bindex = j;
	      break;
	    }
	  }
	  if (found) {
	    // std::cout << "acc += " << a.m_v[i] << "*" << b.m_v[bindex] << std::endl;
	    acc += a.m_v[i]*b.m_v[bindex];
	    // std::cout << "= " << acc << std::endl;
	  }
	}
	if (acc != a.m_zero) {
	  v.push_back(acc);
	  ++rcount;
	  c.push_back(column);
	}
      } // for
      r.push_back(*(r.end()-1)+rcount);
    } // else
  }
  return SMatrix(a.m_N, b.m_M, a.m_zero, v, r, c);
}

std::vector<GFElem> operator*(const std::vector<GFElem> & a, const SMatrix & b) {
  if (a.size() != b.m_N)
    throw std::invalid_argument("sparse vmul: wrong dimentions.");

  std::vector<GFElem> v;
  for (unsigned column = 0; column < b.m_M; ++column) {
    GFElem acc(b.m_zero);
    for (unsigned i = 0; i < a.size(); ++i){
      bool found = false;
      unsigned bindex = 0;
      for (unsigned j = b.m_r[i]; j < b.m_r[i+1]; ++j) {
	if (b.m_c[j] > column)
	  break;
	if (b.m_c[j] == column) {
	  found = true;
	  bindex = j;
	  break;
	}
      }
      if (found) {
	acc += a[i]*b.m_v[bindex];
      }
    }
    v.push_back(acc);
  } // for

  return v;
}

std::vector<GFElem> operator*(const SMatrix & a, const std::vector<GFElem> & b) {
  if (a.m_M != b.size())
    throw std::invalid_argument("sparse mulv: wrong dimentions.");
  
  std::vector<GFElem> v;
  for (unsigned row = 0; row < a.m_r.size()-1; ++row) {
    GFElem acc(a.m_zero);
    // std::cout << "row " << row << std::endl;
    for (unsigned i = a.m_r[row]; i < a.m_r[row+1]; ++i) {
      acc += a.m_v[i]*b[a.m_c[i]];
      // std::cout << "acc += " << a.m_v[i] << "*" << b[a.m_c[i]]  << " = " << acc << std::endl;
    }
    v.push_back(acc);
  }

  return v;
}

SMatrix SMatrix::transpose() const {
  std::vector<GFElem> v(m_v.size());
  std::vector<unsigned> r(m_M+1);
  std::vector<unsigned> c(m_v.size());
  r[0] = 0;
  unsigned vindex = 0;
  unsigned rindex = 1;
  for (unsigned column = 0; column < m_M; ++column) {
    unsigned rcount = 0;
    for (unsigned row = 0; row < m_N; ++row) {
      for (unsigned i = m_r[row]; i < m_r[row+1]; ++i) {
	if (m_c[i] == column) {
	  v[vindex] = m_v[i];
	  c[vindex] = row;
	  ++vindex;
	  ++rcount;
	  break;
	}
      }
    }
    r[rindex] = r[rindex-1] + rcount;
    ++rindex;
  }

  return SMatrix(m_M, m_N, m_zero, v, r, c);
}

std::vector<GFElem> & SMatrix::add(std::vector<GFElem> & acc, const std::vector<GFElem> & b) {
  if (acc.size() != b.size())
    throw std::invalid_argument("vector add: acc.size != b.size.");
  for (unsigned i = 0; i < acc.size(); ++i)
    acc[i] += b[i];
  return acc;
}

std::vector<GFElem> & SMatrix::add(const std::vector<GFElem> & a, const std::vector<GFElem> & b, std::vector<GFElem> & result) {
  result = a;
  add(result, b);
  return result;
}

std::vector<GFElem> & SMatrix::sub(std::vector<GFElem> & acc, const std::vector<GFElem> & b) {
  if (acc.size() != b.size())
    throw std::invalid_argument("vector add: acc.size != b.size.");
  for (unsigned i = 0; i < acc.size(); ++i)
    acc[i] -= b[i];
  return acc;  
}

std::vector<GFElem> & SMatrix::sub(const std::vector<GFElem> & a, const std::vector<GFElem> & b, std::vector<GFElem> & result) {
  result = a;
  sub(result, b);
  return result;
}

void SMatrix::dprod(const std::vector<GFElem> & a, const std::vector<GFElem> & b, GFElem & res) {
  if (a.size() != b.size())
    throw std::invalid_argument("dprod: a.size != b.size.");

  res.Set(0);
  for (unsigned i = 0; i < a.size(); ++i)
    res += a[i]*b[i];
}

bool SMatrix::equals(const std::vector<GFElem> & a, const std::vector<GFElem> & b) {
  if (a.size() != b.size())
    return false;
  bool flag = true;
  for (unsigned i = 0; i < b.size(); ++i) {
    if (a[i] != b[i]) {
      flag = false;
      break;
    }
  }
  return flag;
}

unsigned SMatrix::weight(const GFElem & zero, const std::vector<GFElem> & vec) {
  unsigned w = 0;
  for (auto && el : vec) {
    if (el != zero)
      ++w;
  }
  return w;
}

std::vector<GFElem> SMatrix::cg_method(const SMatrix & A, 
				       const std::vector<GFElem> & b, 
				       const std::vector<GFElem> & x0,
				       const SMatrix & At,
				       const std::vector<GFElem> & AS,
				       int & flag, int & scount) {
  if (A.getN() != b.size())
    throw std::invalid_argument("cg_method: A.m_N != b.size - invalid dimentions.");
  if (A.getN() != x0.size())
    throw std::invalid_argument("cg_method: A.m_N != x0.size - invalid dimentions.");

  // std::cout << "cg-method start: x0 = ";
  // for (auto && el : x0)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  GFElem zero(A.zero());
  // SMatrix At(A.transpose());
  std::vector<GFElem> bt(At*b);

  // SMatrix tmp = At*A;
  // At.printMatrix();
  // std::cout << std::endl;
  // tmp.printMatrix();

  unsigned steps_count = 0;
  std::vector<GFElem> xi(x0);
  std::vector<GFElem> xip1(x0);

  std::vector<GFElem> ri(AS);
  std::vector<GFElem> rip1;

  SMatrix::sub(ri, bt);
  std::vector<GFElem> s(ri);

  GFElem riri(zero);
  SMatrix::dprod(ri,ri,riri);

  // std::cout << "b ";
  // for (auto && el : bt)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  // std::cout << "x0 ";
  // for (auto && el : x0)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  // std::cout << "ri ";
  // for (auto && el : ri)
  //   std::cout << el << " ";
  // std::cout << std::endl;

  // std::cout << "riri " << riri << std::endl;

  while (riri != zero) {
    // std::cout << "step" << steps_count << std::endl;

    GFElem num(riri);
    std::vector<GFElem> As(At*(A*s));
    GFElem den(zero);
    SMatrix::dprod(As, s, den);
    // std::cout << "num " << num << ", den " << den << std::endl;
    if (den == zero) {
      // std::cout << "den == 0 : scount " << steps_count << std::endl;
      scount = steps_count;
      flag = 1;
      return xi;
    }

    GFElem a(num / den);
    SMatrix::add(xi, SMatrix::emulv(a, s), xip1);
    SMatrix::sub(ri, SMatrix::emulv(a, As), rip1);

    // std::cout << "xip1 ";
    // for (auto && el : xip1)
    //   std::cout << el << " ";
    // std::cout << std::endl;    
    // std::cout << "rip1 ";
    // for (auto && el : rip1)
    //   std::cout << el << " ";
    // std::cout << std::endl;
    
    SMatrix::dprod(rip1,rip1, num);
    num = zero - num;
    den = riri;
    
    GFElem bn(num / den);
    // std::cout << "bn " << bn << std::endl;
    SMatrix::sub(rip1, SMatrix::emulv(bn, s), s);

    ri = rip1;
    xi = xip1;
    SMatrix::dprod(ri, ri, riri);
    ++steps_count;
  }
  // std::cout << "cg-method scount : " << steps_count << std::endl;
  scount = steps_count;
  flag = 0;
  return xi;
}

std::vector<GFElem> SMatrix::gen_random_GF_vector(Gen & gen, const GF & field, unsigned n) {
  std::vector<GFElem> ret(n);
  for (unsigned i = 0; i < n; ++i)
    ret[i] = GFElem(field, gen.gen_uint()%2);
  return ret;
}

void SMatrix::printData() const {
  std::cout << "N = " << m_N << ", M = " << m_M << std::endl;
  std::cout << "v = ";
  for (auto el : m_v)
    std::cout << el << " ";
  std::cout << std::endl;
  std::cout << "r = ";
  for (auto el : m_r)
    std::cout << el << " ";
  std::cout << std::endl;
  std::cout << "c = ";
  for (auto el : m_c)
    std::cout << el << " ";
  std::cout << std::endl;
}

bool SMatrix::empty() const {
  return m_v.empty();
}

// std::vector<GFElem> SMatrix::solve_sparse(const SMatrix & A, const std::vector<GFElem> & b, Gen & gen) {
//   if (A.getN() != b.size())
//     throw std::invalid_argument("solve_sparse: A.m_N != b.size - invalid dimentions.");
//   if (b.empty())
//     return std::vector<GFElem>();

//   SMatrix A1(A); // TODO убрать лишнее копирование.
//   const GF * gf = b[0].getGF();
//   std::vector<std::pair<unsigned, unsigned>> correctionA;
//   // Квадрат матрицы равен нулевой матрице.
//   while ((A1*A1).empty()) {
//     // Коррекция: к одному столбцу прибавляем линейную комбинацию других столбцов. 
//     // Решение системы не меняется, однако каждому столбцу соответствует определенное число. 
//     // Сложение столбцов экспонент по модулю 2 равносильно перемножению этих чисел и составлению столбца экспонент их результатов. 
//     // Поэтому заводим дополнительную матрицу для отслеживания этой коррекции.
//     unsigned col1 = gen.gen_uint();
//     unsigned col2 = gen.gen_uint();
//     if (col1 == col2) {
//       ++col2;
//       col2 %= A1.getM();
//     }
//     correctionA.push_back(std::pair<unsigned,unsigned>(col1, col2));
//     A1.add_col(col1, col2);
//   }

//   // TODO Как выбирать начальное приближение?
//   // std::vector<GFElem> x0(b.size(), GFElem(*gf, 111111));
//   std::vector<GFElem> x0(b.size());
//   for (auto && el : x0)
//     el = GFElem(*gf, gen.gen_uint()%gf->getPrimitive());
//   std::vector<GFElem> result(x0.size());

//   bool equal = false;
//   while (!equal) {
//     int flag = 0;
//     int scount = 0;
//     x0 = cg_method(A1, b, x0, flag, scount);

//     // std::cout << "result" << std::endl;
//     // for (auto && el : x0)
//     //   std::cout << el << " ";
//     // std::cout << std::endl;
//     // if (flag == 0) {
//     //   for (auto && el : x0)
//     // 	std::cout << el << " ";
//     //   std::cout << std::endl;
//     // }

//     bool found = false;
//     for (int power = 1; power < 20; ++power) {
//       std::cout << "result.coeffs" << std::endl;
//       for (int i = 0; i < x0.size(); ++i) {
// 	result[i] = x0[i].getCoeff(power);
// 	std::cout << result[i] << " ";
//       }
//       std::cout << std::endl;

//       std::vector<GFElem> b1(A1*result);
//       if (SMatrix::equals(b, b1)) {
// 	flag = true;
// 	break;
//       }
//     }

//     if (flag) {
//       std::cout << "flag!" << std::endl;
//       break;
//     }

//     std::vector<GFElem> b1(A1*result);
//     if (SMatrix::equals(b, b1)) {
//       // A.printMatrix();
//       // std::cout << std::endl;
//       // A1.printMatrix();
//       break;
//     } else {
//       // Коррекция x0.
//       std::vector<GFElem> correction(SMatrix::gen_random_GF_vector(gen, *gf, x0.size()));
//       add(x0, correction);
//       // unsigned index = gen.gen_uint() % x0.size();
//       // x0[index]+=GFElem(*gf, 1);
//       // std::vector<GFElem> x0(b.size());
//       // for (auto && el : x0)
//       // 	el = GFElem(*gf, gen.gen_uint()%gf->getPrimitive());
//       std::cout << "x0" << std::endl;
//       for (auto && el : x0)
//       	std::cout << el << " ";
//       std::cout << std::endl;
//     }
//   }
//   // Учет коррекции матрицы.
//   if (!correctionA.empty())
//     for (unsigned i = 0; i < result.size(); ++i)
//       if (result[i] == GFElem(*gf, 1))
// 	for (unsigned j = 0; j < correctionA.size(); ++j)
// 	  if (correctionA[j].first == i)
// 	    result[correctionA[j].second] += GFElem(*gf, 1);
  
//   return result;
// }

void SMatrix::printMatrix() const {
  const int width = 2;
  std::cout.width(width);
  std::cout << " ";
  for (unsigned i = 0; i < m_M; ++i) {
    std::cout.width(width);
    std::cout << i ;
  }
  std::cout << std::endl;
  for (unsigned i = 0; i < m_N; ++i) {
    unsigned rindex = m_r[i];
    std::cout.width(width);
    std::cout << i ;
    for (unsigned j = 0; j < m_M; ++j) {
      if (rindex < m_r[i+1]) {
	if (j == m_c[rindex]) {
	  std::cout.width(width);
	  std::cout << m_v[rindex];
	  ++rindex;
	} else {
	  std::cout.width(width);
	  std::cout << "0";
	}
      } else {
	std::cout.width(width);
	std::cout << "0";
      }
    }
    std::cout << std::endl;
  }
}

unsigned SMatrix::getHash(const std::vector<GFElem> & vec) {
  unsigned ret = 17;
  for (unsigned i = 0; i < vec.size(); ++i) {
    ret = 37*ret + (unsigned)vec[i];
  }
  return ret;
}
