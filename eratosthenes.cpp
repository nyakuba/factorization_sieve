#include "include/eratosthenes.h"

Eratosthene::Eratosthene(): 
  m_bound(2) {
  m_primes.push_back(-1); m_primes.push_back(1); m_primes.push_back(2);
}
Eratosthene::~Eratosthene(){}

// m_bound = *(m_primes.end()-1) - верхняя граница просеенного промежутка.

void Eratosthene::gen_primes(unsigned bound) {
  if(bound > m_bound) {
    unsigned ssize = bound - m_bound;
    // std::cout << ssize << std::endl;
    std::vector<bool> sieve(ssize, false);
    unsigned psize = m_primes.size();
    for (unsigned i = 2; i < psize; ++i) {
      int prime = m_primes[i];
      unsigned first_index = ((unsigned)(m_bound)/prime)*(prime);
      while (first_index < m_bound) first_index+=prime;
      // std::cout << m_bound << " " << first_index << std::endl;
      for (unsigned j = first_index; j < bound; j+=prime) {
	// std::cout << j - m_bound - 1 << std::endl;
	sieve[j - m_bound] = true;
      }
    }

    for (int j = 0; j <= ((int) sqrt(bound) - (int) m_bound); ++j) {
      if (sieve[j] == false) {
	unsigned step = j+m_bound;
	// std::cout << step << std::endl;
	m_primes.push_back(step);
	for (unsigned k = j+step; k < ssize; k+=step) {
	  // std::cout << k+m_bound << " ";
	  sieve[k] = true;
	}
	// std::cout << std::endl;

      }
    }
    unsigned s = m_primes[m_primes.size()-1]-m_bound+1;
    // std::cout << s << std::endl;
    // std::cout << ssize - s << std::endl;

    for (int i = s; i < ssize; ++i)
      if (sieve[i] == false)
	m_primes.push_back(i+m_bound);
    m_bound = m_primes[m_primes.size()-1];
  }
  // for (auto && el : m_primes)
  //   std::cout << el << " ";
  // std::cout << std::endl;
}

std::vector<int> Eratosthene::form_factor_base(const unsigned & B, const BigNumber & N) {
  std::vector<int> ret(B);
  unsigned rindex = 1;
  gen_primes(1000);
  // std::cout << "1" << std::endl;

  ret[0] = -1;
  for (unsigned i = 2; i < m_primes.size(); ++i) {
    if (BigNumber(m_primes[i]).legendre_symbol(N) == BigNumber::One()) {
      ret[rindex] = m_primes[i]; ++rindex;
      if (rindex == B)
	break;
    }
  }
  // std::cout << "2" << std::endl;
  unsigned last_size = m_primes.size();
  while (rindex < B) {
    // for (auto & el : ret)
    //   std::cout << el << " ";
    // std::cout << std::endl;
    // for (auto & el : m_primes)
    //   std::cout << el << " ";
    // std::cout << std::endl;
    // std::cout << "3" << std::endl;
    gen_primes(m_bound*2);
    for (unsigned i = last_size; i < m_primes.size(); ++i) {
      if (BigNumber(m_primes[i]).legendre_symbol(N) == BigNumber::One()) {
	ret[rindex] = m_primes[i]; ++rindex;
	if (rindex == B)
	  break;
      }
    }
    last_size = m_primes.size();
  }
  return ret;
}
