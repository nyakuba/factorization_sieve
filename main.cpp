// Copyright 2014 nick_yakuba.

#include <ippcp.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <map>
#include <ctime>
#include <pthread.h>
#include <fstream>
#include "include/bignum.h"
#include "include/rand.h"
#include "include/sparse.h"
#include "include/gf.h"
#include "include/gfelem.h"
#include "include/eratosthenes.h"
#include "include/sieve.h"

static bool MSGS = false;
static const unsigned MAX_CG_METHOD_ITERATIONS = 1;
static const unsigned SIEVE_INTERVAL = Sieve::MAX_SIEVE_INTERVAL;
static int NUM_THREADS = 1;

struct thread_sieve_arg {
public:
  const Sieve * sieve;
  int B;
  const std::vector<int> * fb;
  const std::vector<BigNumber> * tsh_fb;
  BigNumber pa;
  Gen * gen;
  std::vector<BigNumber> * smooth;
  std::vector<BigNumber> * acoeff;
  std::vector<std::vector<int> > * exponents;

  void set(const Sieve & s, const int b, 
	   const std::vector<int> & f,
	   const std::vector<BigNumber> & tfb,
	   const BigNumber p, Gen & g,
	   std::vector<BigNumber> & sm,
	   std::vector<BigNumber> & ac,
	   std::vector<std::vector<int> > & exps) {
    sieve = &s; B = b; fb = &f; tsh_fb = &tfb; pa = p; gen = &g; smooth = &sm; acoeff = &ac; exponents = &exps;
  }
};

struct thread_solve_sparse_arg {
public:
  int column;
  const GF * gf;
  const SMatrix * exponents;
  const std::vector<std::pair<unsigned, unsigned> > * correctionA;
  std::vector<GFElem> x0;
  const SMatrix * At;
  const std::vector<GFElem> * AS;
  std::vector<std::vector<GFElem> > * solutions;
  Gen * gen;
  void set(int col,
	   const GF & gfield,
	   const SMatrix & exps, 
	   const std::vector<std::pair<unsigned, unsigned> > & cA, 
	   std::vector<GFElem> X0,
	   const SMatrix & at,
	   const std::vector<GFElem> & as,
	   std::vector<std::vector<GFElem> > & sol,
	   Gen & generator) {
    column = col; gf = &gfield; exponents = &exps; correctionA = &cA; x0 = X0; At = &at; AS = &as; solutions = &sol; gen = &generator;
  }
};

BigNumber qfactor(const BigNumber & N, const unsigned fb_size /* = 0 */, const unsigned sieve_size /* = 0 */,
		  const char * input_filename /* = nullptr */, const char * output_filename /* = nullptr */, 
		  bool filter /* = true */, int filter_count /* = 0 */);
void parseopts(int argc, char * argv[], char ** n, 
	       unsigned & B, unsigned & M, 
	       char ** ifname, char ** ofname, 
	       bool & filter, int & filter_count);
void print_usage();

int main(int argc, char *argv[]) {
  std::cin.sync_with_stdio(false);
  std::cout.sync_with_stdio(false);

  unsigned B = 0, M = 0;
  char * n = nullptr;
  char * input_filename = nullptr;
  char * output_filename = nullptr;
  bool filter = true;
  int filter_count = 0;
  parseopts(argc, argv, &n, B, M, &input_filename, &output_filename, filter, filter_count);

  BigNumber N;
  if (n == nullptr && input_filename == nullptr) {
    print_usage();
    return 0;
  } else if (n != nullptr) {
    N = BigNumber(n);
    std::cout << N.BitSize() << " bits size." << std::endl;
    std::cout << "N = " << N << std::endl;
  } else {
    N = BigNumber::Zero();
  }

  BigNumber factor = BigNumber::One();

  time_t start = time(NULL);
  factor = qfactor(N, B, M, input_filename, output_filename, filter, filter_count);
  time_t end = time(NULL);
  std::cout << "Time : " << end - start << std::endl;

  std::cout << "N = " << N << std::endl;
  std::cout << "factor = " << factor << std::endl;

  if (N % factor == BigNumber::Zero())
    std::cout << "factored!" << std::endl;
  else
    std::cout << "error!" << std::endl;

  return 0;
}

static void * sieve_mp(void *);
static void * solve_sparse(void * arg0);

BigNumber qfactor(const BigNumber & num, const unsigned fb_size = 0, const unsigned sieve_size = 0, 
		  const char * input_filename = nullptr, const char * output_filename = nullptr, 
		  bool filter = true, int filter_count = 0) {
  if (MSGS)
    std::cout << "qfactor start: " << num << std::endl;

  time_t start = time(NULL);

  Gen gen;
  // GF gf(7, 137); // x^7 + x^3 + 1.
  // GF gf(11, 2053); // x^11 + x^2 + 1.
  // GF gf(20, 1048585); // x^20 + x^3 + 1.
  GF gf(24, 16777351); // x^24 + x^7 + x^2 + x + 1.
  BigNumber N;
  unsigned B;
  std::vector<int> fb;
  std::vector<BigNumber> smooth;
  std::vector<BigNumber> acoeff;
  SMatrix exponents(0,0,GFElem(gf, 0));
  Sieve sieve;

  // Определяем границу гладкости.
  if (input_filename == nullptr) {
    N = num;
    sieve.setN(N);

    if (gen.test_prime(50, N))
      return N;

    // if (N.is_perfect_square()) {
    //   BigNumber num(N); num.bsqrt();
    //   return num;
    // }

    double l = loge(N);
    // unsigned B = exp(sqrt(l*log(l))/2);
    B = (fb_size == 0)? 5*sqrt(exp(sqrt(l*log(l))/2)) : fb_size;
    if (MSGS) {
      std::cout << "B " << B << std::endl;
      std::cout << "form factor base" << std::endl;
    }
    // Формируем базу простых делителей.
    Eratosthene primes;
    fb = primes.form_factor_base(B, N);
    std::vector<BigNumber> tsh_fb; tsh_fb.push_back(BigNumber::Zero());
    for (int i = 1; i < B; ++i) {
      tsh_fb.push_back(Sieve::tonelli_shanks(N, fb[i]));
    }

    //*** Debug ***//
    // std::cout << "fb: ";
    // for (auto & el : tsh_fb)
    //   std::cout << el << " ";
    // std::cout << std::endl;
    //*************//
    if (MSGS)
      std::cout << "sieving" << std::endl;
    // Просеивание.
    exponents.setN(fb.size());

    // Выберем сначала первое р такое, что M = 4096*16;
    // И нагенерим кучу р.
    unsigned M = (sieve_size == 0)? SIEVE_INTERVAL : sieve_size;
    BigNumber firstp(2*N); firstp.bsqrt(); 
    firstp /= M; firstp.bsqrt();
    if (firstp.isDivTwo()) {
      firstp += BigNumber::One();
    }
    if (firstp <= BigNumber(3))
      firstp = BigNumber(3);

    //*** Debug ***//
    if (MSGS)
      std::cout << "firstp lower_bound: " << firstp << std::endl;
    //*************//

    std::vector<pthread_t> threads(NUM_THREADS);
    std::vector<struct thread_sieve_arg> targs(NUM_THREADS);
    int thread_index = 0;

    std::vector<std::vector<BigNumber> > thread_smooth(NUM_THREADS);
    std::vector<std::vector<BigNumber> > thread_acoeff(NUM_THREADS);
    std::vector<std::vector<std::vector<int> > > thread_exps(NUM_THREADS);
    while (smooth.size() < B) {
      for (auto && vec : thread_smooth)	vec.clear();
      for (auto && vec : thread_acoeff)	vec.clear();
      for (auto && vec : thread_exps) vec.clear();
      while (thread_index < NUM_THREADS) {
	while (!gen.test_prime(50, firstp)) {	  
	  // std::cout << firstp << std::endl;
	  firstp += BigNumber::Two();
	}
	if (firstp.legendre_symbol(N) == 1) {
	  //*** Debug ***//
	  // std::cout << "prime: " << std::dec << firstp.uns() << std::endl;
	  //*************//
	  targs[thread_index].set(sieve, B, fb, tsh_fb, firstp, gen, thread_smooth[thread_index], 
				  thread_acoeff[thread_index], thread_exps[thread_index]);
	  if(0 != pthread_create(&threads[thread_index], NULL, &sieve_mp, &targs[thread_index]))
	    std::cerr << "thread_create error" << std::endl;

	  ++thread_index;
	}
	firstp += BigNumber::Two();
      }

      for (int i = 0; i < NUM_THREADS; ++i) {
	int s = pthread_join(threads[i], NULL);
	if (0 != s) {
	  switch (s) {
	  case ESRCH : std::cerr << "join error: no thread with such id" << std::endl; break;
	  case EINVAL : std::cerr << "join error: thread is not joinable" << std::endl; break;
	  case EDEADLK : std::cerr << "join error: deadlock" << std::endl; break;
	  default: std::cout << "undefined error" << std::endl;
	  }
	  return N;
	}
      }

      // std::cout << "result union" << std::endl;

      // Объединение результатов.
      for (int i = 0; i < NUM_THREADS && smooth.size() < B; ++i) {
	for (int j = 0; j < thread_smooth[i].size() && smooth.size() < B; ++j) {
	  smooth.push_back(thread_smooth[i][j]);
	  acoeff.push_back(thread_acoeff[i][j]);
	  for (auto && el : thread_exps[i][j])
	    el %= 2;
	  exponents.append(thread_exps[i][j]);
	}
      }
   
      thread_index = 0;
      if (MSGS)
	std::cout << "found " << smooth.size() << " smooth." << std::endl;
    }

    if (output_filename != nullptr) {
      std::ofstream out(output_filename);
      if (!out.good()) {
	std::cout << "error open output file" << std::endl;
      } else {
	out << N << std::endl;
	out << B << std::endl;
	for (int i = 0; i < B; ++i)
	  out << fb[i] << " ";
	out << std::endl << std::endl;
	for (int i = 0; i < B; ++i)
	  out << smooth[i] << " ";
	out << std::endl << std::endl;
	for (int i = 0; i < B; ++i)
	  out << acoeff[i] << " ";
	out << std::endl << std::endl;
	for (int i = 0; i < B; ++i) {
	  for (int j = 0; j < B; ++j) {
	    out << exponents.get(j,i) << " ";
	  }
	  out << std::endl;
	}
	out.close();
      }
    }
  } else { // input_filename != nullptr 
    std::ifstream in(input_filename);
    if (!in.good()) {
      std::cout << "error: open file " << input_filename << std::endl;
      return BigNumber::One();
    }
    std::string nstr;
    in >> nstr; N = BigNumber(nstr.c_str());
    sieve.setN(N);
    if (MSGS) {
      std::cout << N.BitSize() << " bits size." << std::endl;
      std::cout << "N = " << N << std::endl;
    }
    in >> B;
    for (int i = 0; i < B; ++i) {
      int factor; in >> factor;
      fb.push_back(factor);
    }
    exponents.setN(fb.size());
    for (int i = 0; i < B; ++i) {
      std::string sm; in >> sm;
      smooth.push_back(BigNumber(sm.c_str()));
    }
    for (int i = 0; i < B; ++i) {
      std::string ac; in >> ac;
      acoeff.push_back(BigNumber(ac.c_str()));
    }
    for (int i = 0; i < B; ++i) {
      std::vector<int> exp;
      for (int j = 0; j < B; ++j) {
	int e; in >> e;
	exp.push_back(e);
      }
      exponents.append(exp);
    }
    in.close();
  }

  std::cout << "Sieving: " << time(NULL) - start << std::endl;

  //*** Debug ***//
  // std::cout << "smooth : " << std::endl;
  // for (auto && el : smooth)
  //   std::cout << el << " ";
  // std::cout << std::endl;
  // std::cout << "acoeff" << std::endl;
  // for (auto && el : acoeff)
  //   std::cout << el << " ";
  // std::cout << std::endl;
  // exponents.printData();
  // std::cout << "matrix" << std::endl;
  // exponents.printMatrix();
  //*************//

  // Фильтрация
  if(MSGS)
    std::cout << "filter exp matrix" << std::endl;

  // exponents.printMatrix();
  if (filter) {
    bool filtered = true;
    if (filter_count > 0) {
      for (int i = 0; i < filter_count && filtered; ++i) {
	filtered = exponents.filter(smooth, acoeff, fb);
      }
    } else {
      while (filtered) {
	filtered = exponents.filter(smooth, acoeff, fb);
      }
    }
  }
  // exponents.printMatrix();
  B = fb.size();

  if(MSGS)
    std::cout << "B = " << B << std::endl;
  
  // exponents.printData();

  std::vector<std::pair<unsigned, unsigned>> correctionA;
  // Квадрат матрицы равен нулевой матрице.
  while ((exponents*exponents).empty()) {
    // Коррекция: к одному столбцу прибавляем линейную комбинацию других столбцов. 
    // Решение системы не меняется, однако каждому столбцу соответствует определенное число. 
    // Сложение столбцов экспонент по модулю 2 равносильно перемножению этих чисел и составлению столбца экспонент их результатов. 
    // Поэтому заводим дополнительную матрицу для отслеживания этой коррекции.
    unsigned col1 = gen.gen_uint();
    unsigned col2 = gen.gen_uint();
    if (col1 == col2) {
      ++col2;
      col2 %= exponents.getM();
    }
    exponents.add_col(col1, col2);
    correctionA.push_back(std::pair<unsigned,unsigned>(col1, col2));
  }

  // NUM_THREADS = 1;
  std::vector<pthread_t> threads(NUM_THREADS);
  int thread_index = 0;
  std::vector<struct thread_solve_sparse_arg> thread_ss_args(NUM_THREADS);
  std::vector<std::vector<std::vector<GFElem> > > thread_solutions(NUM_THREADS);
  // TODO Как выбирать начальное приближение?
  // Начальное приближение для всех будет общим.
  std::vector<GFElem> x0(exponents.getN());
  SMatrix At(exponents.transpose());
  for (auto && el : x0) {
    // el = GFElem(gf, 1111111111);
    el = GFElem(gf, gen.gen_uint()%gf.getPrimitive());
  }
  std::vector<GFElem> AS(At*(exponents*x0));
  // Поиск ЛЗ системы векторов.  
  time_t ssparse_start;
  for (int column_index = 0; column_index < exponents.getM(); column_index+=NUM_THREADS) {
    ssparse_start = time(NULL);
    if (MSGS)
      std::cout << "solve sparse iteration" << std::endl;

    thread_index = 0;
    for (int i = 0; i < NUM_THREADS && i + column_index < exponents.getM(); ++i) {
      thread_ss_args[i].set(column_index+i, gf, exponents, correctionA, x0, At, AS, thread_solutions[i], gen);
      if(0 != pthread_create(&threads[thread_index], NULL, &solve_sparse, &thread_ss_args[thread_index]))
	std::cerr << "thread_create error" << std::endl;
 
      ++thread_index;
    }

    for (int i = 0; i < thread_index; ++i) {
      int s = pthread_join(threads[i], NULL);
      if (0 != s) {
	switch (s) {
	case ESRCH : std::cerr << "join error: no thread with such id" << std::endl; break;
	case EINVAL : std::cerr << "join error: thread is not joinable" << std::endl; break;
	case EDEADLK : std::cerr << "join error: deadlock" << std::endl; break;
	default: std::cout << "undefined error" << std::endl;
	}
	return N;
      }
    }

    std::vector<std::vector<GFElem> *> solutions;
    for (int i = 0; i < thread_index; ++i) {
      for (auto && s : thread_solutions[i]) {
	solutions.push_back(&s);
      }
    }

    if(MSGS) {
      std::cout << "Time : " << time(NULL) - ssparse_start << std::endl;
      std::cout << "found " << solutions.size() << " solutions." << std::endl;
    }

    std::vector<BigNumber> lz_nums;
    std::vector<BigNumber> lz_acoeffs;
    for (auto && solution : solutions) {
      //*** Debug ***//
      // std::cout << "solution : ";
      // for (auto && el : solution)
      // 	std::cout << el << " ";
      // std::cout << std::endl;
      //*************//

      // Тривиальное решение?
      if (SMatrix::weight(GFElem(gf, 0), *solution) == 0) {
	if (MSGS)
	  std::cout << "trivial" << std::endl;
	continue;
      }
      if (MSGS)
	std::cout << "check lz_nums" << std::endl;
      // Теперь проверим, подходит ли найденная линейная зависимость.
      // 1. Вычисляем произведение гладких чисел.
      // 2. Факторизуем произведение Q(x) пробным делением.
      //    Нас интересует суммарная степень каждого множителя.
      // 3. Сравнимо ли произведение с +- корнем из него по модулю N?
      //    Да - все плохо.
      //    Нет - все ок.
      lz_nums.clear();
      lz_acoeffs.clear();
      BigNumber product(1U);
      for (unsigned i = 0; i < solution->size(); ++i) {
	if ((*solution)[i] == GFElem(gf, 1)) {
	  lz_nums.push_back(smooth[i]);
	  lz_acoeffs.push_back(acoeff[i]);
	  product *= smooth[i];
	}
      }
      product %= N;

      //*** Debug ***//
      // std::cout << "lz_nums: ";
      // for (auto && el : lz_nums)
      // 	std::cout << el << " ";
      // std::cout << std::endl;
      // std::cout << "lz_acoeffs: ";
      // for (auto && c : lz_acoeffs)
      // 	std::cout << c << " "; 
      // std::cout << std::endl;
      //*************//

      BigNumber sqrt(1U);
      bool factored = false;
      std::vector<unsigned> pows(fb.size(), 0);
      for (unsigned i = 0; i < lz_nums.size(); ++i) {
	BigNumber qx(lz_nums[i]*lz_nums[i] - N);
	std::vector<int> e;
	factored = sieve.pdiv(qx, fb, e);
	if (!factored) {
	  // std::cout << "!factored : " << qx << " " << acoeff[i] << std::endl;
	  sqrt *= lz_acoeffs[i];
	}
	for (unsigned i = 0; i < e.size(); ++i)
	  pows[i]+=e[i];
      }
    
      for (auto && el : pows) {
	// std::cout << el << std::endl;
	el /= 2;
      }
  
      for (unsigned i = 0; i < pows.size(); ++i) {
	BigNumber tmp(fb[i]);
	tmp.bpow(pows[i]);
	sqrt *= tmp;
      }
      sqrt %= N;
      
      //*** Debug ***//
      if (MSGS) {
	std::cout << "product" << product << std::endl;
	std::cout << "sqrt " << sqrt << std::endl;
      }
      //*************//
  
      if ((product-sqrt) % N == BigNumber::Zero() || 
	  (product+sqrt) % N == BigNumber::Zero() ) {
	if (MSGS)
	  std::cout << "product +- sqrt mod N = 0" << std::endl;
	continue;
      } else {
	if (MSGS)
	  std::cout << "calculating gcd" << std::endl;
	BigNumber gcd(N);
	bgcd(product - (sqrt % N), N, gcd);
	if (MSGS)
	  std::cout << "gcd " << gcd << std::endl;
	return gcd;
      }
    } // цикл по solutions
  } // цикл по column_index
  return BigNumber::One();
}

void parseopts(int argc, char * argv[], char ** n, unsigned & B, 
	       unsigned & M, char ** ifname, char ** ofname, bool & filter, int & filter_count) {
  if (argc > 1) {
    for (int i = 1; i < argc; ++i) {
      if (strcmp(argv[i], "-N") == 0 && i+1 < argc) {
	*n = argv[i+1];
      } else if (strcmp(argv[i], "-B") == 0 && i+1 < argc) {
	B = atoi(argv[i+1]);
	std::cout << "B = " << B << std::endl;
      } else if (strcmp(argv[i], "-M") == 0 && i+1 < argc) {
	M = atoi(argv[i+1]);
	std::cout << "M = " << M << std::endl;
      } else if (strcmp(argv[i], "-nt") == 0 && i+1 < argc) {
	int nt = atoi(argv[i+1]);
	if (nt > 0) NUM_THREADS = nt;
	std::cout << "NUM_THREADS = " << NUM_THREADS << std::endl;
      } else if (strcmp(argv[i], "-i") == 0 && i+1 < argc) {
	*ifname = argv[i+1];
      } else if (strcmp(argv[i], "-o") == 0 && i+1 < argc) {
	*ofname = argv[i+1];
      } else if (strcmp(argv[i], "-msg") == 0) {
	MSGS = true;
      } else if (strcmp(argv[i], "-no-filter") == 0) {
	std::cout << "no filter" << std::endl;
	filter = false;
      } else if (strcmp(argv[i], "-filter") == 0) {
	filter = true;
	filter_count = atoi(argv[i+1]);
      }
    }
  }
}

void print_usage() {
  std::cout << "Usage: -N[number] [-B[smooth bound] -M[sieve interval] "
	    << "-nt[num threads] -msg -i[input filename] -o[output filename] -filter]." << std::endl;
}

static void * sieve_mp(void * arg0) {
  struct thread_sieve_arg *arg = (struct thread_sieve_arg *) arg0;
  arg->sieve->gen_smooth_mp(arg->B, *arg->fb, *arg->tsh_fb, arg->pa, *arg->gen, 
			   *arg->smooth, *arg->acoeff, *arg->exponents);
  return 0;
}

void solve_sparse(int column_index,
		  const GF & gf,
		  const SMatrix & exponents, 
		  const std::vector<std::pair<unsigned, unsigned> > & correctionA, 
		  const SMatrix & At,
		  const std::vector<GFElem> & AS,
		  std::vector<std::vector<GFElem> > & solutions,
		  Gen & gen,
		  std::vector<GFElem> & x0) {
  std::vector<GFElem> b(exponents.getColumn(column_index));
  std::vector<GFElem> solution(b.size());
  std::map<unsigned, int> shash;
  solutions.clear();
  
  //*** Debug ***//
  // std::cout << "b" << std::endl;
  // for (auto && el : b)
  //   std::cout << el << " ";
  // std::cout << std::endl;
  //*************//

  // Цикл вызова cg_method
  for (unsigned iteration_index = 0; 
       iteration_index < MAX_CG_METHOD_ITERATIONS; 
       ++iteration_index) {
    int flag = 0;
    int scount = 0;
    x0 = SMatrix::cg_method(exponents, b, x0, At, AS, flag, scount);

    if (MSGS)
      std::cout << "cg_method iterations: " << scount << std::endl;
      

    //*** Debug ***//
    // std::cout << "x0" << std::endl;
    // for (auto && el : x0)
    // 	std::cout << el << " ";
    // std::cout << std::endl;
    //*************//

    bool equals = false;
    for (int power = 0; power < gf.getM(); ++power) {
      for (int i = 0; i < x0.size(); ++i) {
	solution[i] = x0[i].getCoeff(power);
      }

      //*** Debug ***//
      // std::cout << "solution.coeffs" << std::endl;
      // for (int i = 0; i < x0.size(); ++i) {
      // 	std::cout << solution[i] << " ";
      // }
      // std::cout << std::endl;
      //*************//

      std::vector<GFElem> b1(exponents*solution);

      // std::cout << "b0" << " ";
      // for (auto && el : b)
      //   std::cout << el << " ";
      // std::cout << std::endl;
      // std::cout << "b1" << " ";
      // for (auto && el : b1)
      //   std::cout << el << " ";
      // std::cout << std::endl;

      if (SMatrix::equals(b, b1)) {
	equals = true;
	unsigned hash = SMatrix::getHash(solution);
	if (shash.find(hash) == shash.end()) {
	  shash.insert(std::pair<unsigned, int>(hash, power));
	}
      }
    } // power

    if (equals) {
      // std::cout << "flag!" << std::endl;
      break;
    } else {
      // Коррекция x0.
      if (flag == 1) {
	std::vector<GFElem> correction(SMatrix::gen_random_GF_vector(gen, gf, x0.size()));
	SMatrix::add(x0, correction);
      } else {
	for (int i = 0; i < x0.size(); ++i)
	  x0[i] = GFElem(gf, gen.gen_uint()%gf.getPrimitive());
      }
    }
  } // for cg_method

    //*** Debug ***//
    //*************//
  // if (MSGS) {
  //   std::cout << "shash : " << std::endl;
  //   for (auto && el : shash)
  //     std::cout << el.first << " " << el.second << std::endl;
  // }

  while (!shash.empty()) { // Цикл по найденным решениям.
    int power = (*shash.begin()).second;
    shash.erase(shash.begin());
    for (int i = 0; i < x0.size(); ++i) {
      solution[i] = x0[i].getCoeff(power);
    }

    // Учет коррекции матрицы.
    if (!correctionA.empty())
      for (unsigned i = 0; i < solution.size(); ++i)
	if (solution[i] == GFElem(gf, 1))
	  for (unsigned j = 0; j < correctionA.size(); ++j)
	    if (correctionA[j].first == i)
	      solution[correctionA[j].second] += GFElem(gf, 1);

    // Коррекция матрицы (учет столбца свободных членов).
    solution[column_index] += GFElem(gf, 1);

    solutions.push_back(solution);
  }
}

void * solve_sparse(void * arg0) {
  struct thread_solve_sparse_arg * arg = (struct thread_solve_sparse_arg *) arg0;
  solve_sparse(arg->column, *arg->gf, *arg->exponents, *arg->correctionA, *arg->At, *arg->AS, *arg->solutions, *arg->gen, arg->x0);
  return 0;
}

void testfilter() {
  // // TEST

  // GF gf(4,19);
  // GFElem zero(gf, 0);
  // unsigned N = 6;
  // unsigned M = 7;
  // std::vector<GFElem> v = {GFElem(gf, 5), GFElem(gf, 1), GFElem(gf, 6), GFElem(gf, 6), GFElem(gf, 7), 
  // 			   GFElem(gf, 4), GFElem(gf, 2), GFElem(gf, 8), GFElem(gf, 3), GFElem(gf, 9)};
  // /* 5 0 0 1 0 0 0
  //    0 0 6 0 0 6 0
  //    0 0 0 0 0 0 0
  //    7 4 0 2 0 0 8
  //    0 0 0 0 0 0 0
  //    0 0 0 0 3 0 9 */
  // std::vector<unsigned> r = {0, 2, 4, 4, 8, 8, 10};
  // std::vector<unsigned> c = {0, 3, 2, 5, 0, 1, 3, 6, 4, 6};  
  // SMatrix matrix(N,M,zero,v,r,c);
  // matrix.printMatrix();
  // matrix.printData();

  // matrix.rm_rc(3,6);
  // matrix.printMatrix();
  // matrix.printData();

  // return 0;

  // // TEST
}
