#lang racket
(require racket/math)

(define (pow num power)
  (define (pow-r num value power)
    (cond ((<= power 1) num)
          (else (pow-r (* num value) value (- power 1)))))
  (pow-r num num power))

(define (h2d digits)
  (define (h2d-rec digits size sum)
    (cond ((empty? digits) sum)
          (else (h2d-rec (cdr digits) 
                         (- size 1) 
                         (+ sum (* (pow 16 size) 
                                   (car digits)))))))
  (h2d-rec digits (- (length digits) 1) 0))
              