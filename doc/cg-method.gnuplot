set terminal png
set title ""
set output "cg-method.png"
set xlabel "Matrix dimension"
set ylabel "Time, sec."
set xrange [0:250]
set yrange [0:10000]
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
plot "sieve_table.tex" using 1:3 title "" w lp ls 5