set terminal png
set title ""
set output "sieve.png"
set logscale y
set xlabel "Number size, bit"
set ylabel "Time, sec."
set xrange [70:190]
set yrange [1:10000]
set style line 5 lt 1 lw 1 lc rgb "blue" pi -1 pt 7 ps 1.5
plot "sieve_table.tex" using 1:3 title "" w lp ls 5
